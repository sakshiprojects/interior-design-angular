import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { LocalStorage } from '../shared/localstorage.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private localStorage: LocalStorage,
    private location: Location,
    private router: Router,
    private snackBar: MatSnackBar
    ) {}

  get isLoggedIn(): boolean {
   const accessToken = this.localStorage.getAccessToken();
  //  console.log('=======Access Token in Main Component======', accessToken);
   return accessToken ? true : false;
  }

  onLogout(): void {
    this.localStorage.clearAccessToken();
    this.router.navigate(['/login']);
    this.snackBar.open('Logged out Successfully!', 'Close', {
      duration: 1500,
    });
  }

  // onProjectChange(): void {
  //   this.location.replaceState('/project');
  // }

  onProfileChange(): void {
    this.location.replaceState('/profile');
  }






  //   private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  //   get isLoggedIn() {
  //   // return this.loggedIn.asObservable();
  //   return true;
  // }

  // isLoggedIn$: Observable<boolean>;

  // ngOnInit(): void {
  //   // this.isLoggedIn$ = this.isLoggedIn;
  // }

}

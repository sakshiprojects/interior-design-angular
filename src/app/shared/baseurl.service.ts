import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class BaseUrl {
     baseURL = 'http://localhost:3000/dev/';
    constructor() {}

    getFinalUrl(endPoint): string {
        return this.baseURL + endPoint;
    }
}

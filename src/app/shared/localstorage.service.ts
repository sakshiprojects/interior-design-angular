import { Injectable, OnInit } from '@angular/core';

@Injectable({providedIn: 'root'})
export class LocalStorage {

    constructor() {}

    setAccessToken(token: string): void {
        localStorage.setItem('token', token);
    }

     getAccessToken(): string {
        return localStorage.getItem('token');
    }

    clearAccessToken(): void {
        localStorage.clear();
    }
}

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { UploadFileDataRequestModel } from '../bank-account-data/uploadfiledata';

@Injectable({providedIn: 'root'})
export class UploadFileService {
    constructor(private http: HttpClient, private baseUrl: BaseUrl, private localStorage: LocalStorage) {}

    public getToken = this.localStorage.getAccessToken();

    uploadFile(uploadFileData: UploadFileDataRequestModel): Observable<any> {
        return this.http.post(this.baseUrl.getFinalUrl('user/file'), uploadFileData,
        {
            headers: new HttpHeaders({ 'Authorization' : this.getToken })
        }
        );
    }

    getBankDetailFiles(fileType: number, designCafeEmail: string): Observable<any> {
        const param = new HttpParams().set('fileType', fileType.toString()).set('designCafeEmail', designCafeEmail);
        const header = new HttpHeaders({'Authorization': this.getToken});
        return this.http.get(this.baseUrl.getFinalUrl('user/file') , {params: param, headers: header});
    }
}
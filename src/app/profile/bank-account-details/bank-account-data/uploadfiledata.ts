export class UploadFileDataRequestModel {
    public fileName: string;
    public contentType: string;
    public base64File: string;
    public fileType: number;
}
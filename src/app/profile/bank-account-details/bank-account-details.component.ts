import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgModel, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { ProfileData } from '../profile-data/profiledata';
import { ProfileService } from '../service/profile.service';
import { ToastrService } from 'ngx-toastr';
import { UploadFileService } from './bank-account-service/uploadfile.service';
import { UploadFileDataRequestModel } from './bank-account-data/uploadfiledata';

@Component({
  selector: 'app-bank-account-details',
  templateUrl: './bank-account-details.component.html',
  styleUrls: ['./bank-account-details.component.css']
})
export class BankAccountDetailsComponent implements OnInit, OnDestroy {
  bankAccountDetailsForm: FormGroup;
  public profileData: ProfileData;
  bankData: any = '';
  isLoading = false;
  subscription: Subscription;
  selectedFile: File = null;
  base64textString: string = null;
  fileName: string = null;
  currentEditingFile: number = null;
  contentType: string = null;
  panCard: string = null;
  panCardDisplayName: string = null;
  aadharCard: string = null;
  aadharCardDisplayName: string = null;
  gstCertificate: string = null;
  gstCertificateDisplayName: string = null;
  passbook: string = null;
  passbookDisplayName: string = null;
  // currentUploadFile: string = null;
  public officialEmail: string = null;
  // public uploader: FileUploader = new FileUploader({ url: uploadAPI, itemAlias: 'file' });

  constructor(
    private profileService: ProfileService,
    private uploadFileService: UploadFileService,
    private snackBar: MatSnackBar,
    private localStorage: LocalStorage,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.officialEmail = JSON.parse(localStorage.getItem('officialEmail'));
    this.onGetBankDetails();
    // this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    // this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    //      console.log('FileUpload:uploaded successfully:', item, status, response);
    //      alert('Your file has been uploaded successfully');
    // };
  }

  onSubmit(bankData): void {
    this.isLoading = true;
    this.subscription = this.profileService.updateProfileData(bankData).subscribe(bankDetails => {
      this.isLoading = false;
      console.log('-------Response(Callback):', bankDetails);
      if (bankDetails.code === 200) {
      this.onGetBankDetails();
    } else if (bankDetails.code === 400) {
      // Alert => profileData.message
      // alert(bankDetails.message);
      this.snackBar.open(bankDetails.message, 'Close', {
        duration: 1500,
      });
    }
    },
    errorMessage => {
      console.log("CORS Error",errorMessage);
      if(errorMessage.statusText == "Unknown Error"){
      this.toastr.error('Some unknown error occured. Please contact administrator');
      this.isLoading = false
    }
    }
    );
    console.log(this.bankAccountDetailsForm);
  }

  onGetBankDetails(): void {
    this.isLoading = true;
    this.subscription = this.profileService.getProfileData().subscribe(bankDetails => {
      this.isLoading = false;
      console.log('---------Before Data:', this.profileData);
      if (bankDetails.code === 200) {
        this.profileData = bankDetails.userObj;
      } else if (bankDetails.code >= 400) {
        this.localStorage.clearAccessToken();
        this.router.navigate(['/login']);
        this.snackBar.open('Session Expired', 'close', {
          duration: 1500,
        });
      }
      this.bankAccountDetailsForm = new FormGroup({
        'nameAsPerBankAcc': new FormControl(this.profileData.nameAsPerBankAcc, Validators.required),
        'ifscCode': new FormControl(this.profileData.ifscCode, Validators.required),
        'bankName': new FormControl(this.profileData.bankName, Validators.required),
        'panTanNumber': new FormControl(this.profileData.panTanNumber, Validators.required),
        'accountNumber': new FormControl(this.profileData.accountNumber, Validators.required),
        'aadharNumber': new FormControl(this.profileData.aadharNumber, Validators.required),
        'branchName': new FormControl(this.profileData.branchName, Validators.required),
        'gstNumber': new FormControl(this.profileData.gstNumber, Validators.required),
        'designCafeEmail': new FormControl(this.profileData.designCafeEmail)
      });
      console.log('---------After Data:', this.profileData);
    },
    errorMessage => {
      console.log("CORS Error",errorMessage);
      if(errorMessage.statusText == "Unknown Error"){
      this.toastr.error('Some unknown error occured. Please contact administrator');
      this.isLoading = false
    }
    }
    );
     this.getBankDetailsFiles(1);
     this.getBankDetailsFiles(2);
     this.getBankDetailsFiles(3);
     this.getBankDetailsFiles(4);
    //  this.aadharCard  =  this.getBankDetailsFiles(2);
    //  this.gstCertificate = this.getBankDetailsFiles(3);
    //  this.passbook = this.getBankDetailsFiles(4);
     
  }
   
  getBankDetailsFiles(filetype) {
    
    this.subscription = this.uploadFileService.getBankDetailFiles(filetype, this.officialEmail).subscribe(response => {
      if (response.code == 200) {
        // console.log("Filetype ", filetype, "location", response.fileLocation);
        if (filetype == 1) {
          this.panCard = response.fileLocation;
          this.panCardDisplayName = response.fileName;
        } else if (filetype == 2) {
          this.aadharCard = response.fileLocation;
          this.aadharCardDisplayName = response.fileName;
        } else if (filetype == 3) {
          this.gstCertificate = response.fileLocation;
          this.gstCertificateDisplayName = response.fileName;
        } else if (filetype == 4) {
          this.passbook = response.fileLocation;
          this.passbookDisplayName = response.fileName;
        }
        // console.log("PAN CARD URL: ", this.panCard);
        // return response.body.fileLocation;
      }
    });
  }

  onFileSelected(event, fileType): void {
    // debugger
    this.currentEditingFile = fileType
    console.log(event.target.files[0]);
    const files = event.target.files
    var file = files[0];
    if (files && file) {
        var reader = new FileReader();
        reader.onload =this.handleFile.bind(this);
        reader.readAsBinaryString(file);
        this.fileName = file.name;
        this.contentType = file.type;
console.log("----->>>>>>>>>", fileType, typeof(fileType), fileType == 1);

        if (fileType == 1) {
          this.panCardDisplayName = file.name;
          console.log("Inside onFileSelected for filetype", fileType, "fileName:", file.name, "Saved Name:", this.panCardDisplayName);
        } else if (fileType == 2) {
          this.aadharCardDisplayName = file.name;
          console.log("Inside onFileSelected for filetype", fileType, "fileName:", file.name, "Saved Name:", this.aadharCardDisplayName);
        } else if (fileType == 3) {
          this.gstCertificateDisplayName = file.name;
          console.log("Inside onFileSelected for filetype", fileType, "fileName:", file.name, "Saved Name:", this.gstCertificateDisplayName);
        } else if (fileType == 4) {
          this.passbookDisplayName = file.name;
          console.log("Inside onFileSelected for filetype", fileType, "fileName:", file.name, "Saved Name:", this.passbookDisplayName);
        }
    }
  }

  handleFile(event) {
    var binaryString = event.target.result;
    this.base64textString= btoa(binaryString);
    // console.log(this.base64textString);
   }

  uploadFile(filetype) {
    if (this.fileName && this.contentType && this.base64textString && filetype && this.currentEditingFile == filetype) {
      const requestUploadfileData: UploadFileDataRequestModel = new UploadFileDataRequestModel();
      requestUploadfileData.fileName = this.fileName;
      requestUploadfileData.contentType = this.contentType;
      requestUploadfileData.base64File = this.base64textString;
      requestUploadfileData.fileType = filetype;
      // if (this.currentUploadFile == filetype) {
      //   this.currentUploadFile = filetype;
      // } else {
      //   this.toastr.error('....');
      // }
      this.isLoading = true;
      this.subscription = this.uploadFileService.uploadFile(requestUploadfileData).subscribe(response => {
        this.isLoading = false;
        if (response.code === 200) {
          this.onGetBankDetails();
          this.fileName = null;
          this.contentType = null;
          this.base64textString = null;
          this.snackBar.open('File Uploaded Successfully', 'Close', {
            duration: 1500,
          });
        } else {
          this.snackBar.open(response.message, 'Close', {
            duration: 1500,
          });
        }
      },
      errorMessage => {
        console.log("CORS Error",errorMessage);
        if(errorMessage.statusText == "Unknown Error"){
        this.toastr.error('Some unknown error occured. Please contact administrator');
        this.isLoading = false
      }
      }
      );
    } else {
      // alert("Please sselect a file")
      this.toastr.error('Please select a file..');
    }
  }
  openSnackBar(): void {
    this.snackBar.open('Saved Successfully!', 'close', {
      duration: 1200,
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}












// this.bankAccountDetailsForm = new FormGroup({
    //   'name': new FormControl(null),
    //   'ifscCode': new FormControl(null),
    //   'bankName': new FormControl(null),
    //   'panNumber': new FormControl(null),
    //   'accountNumber': new FormControl(null),
    //   'aadharNumber': new FormControl(null),
    //   'branchName': new FormControl(null),
    //   'gstNumber': new FormControl(null)
    // });


    // this.selectedFile = (event.target.files[0] as File);
    // console.log('======Show Result data=====', this.selectedFile);
    // this.selectedFile = (event.target.files[0].name as File);
    // console.log('======Show Result name=====', this.selectedFile);
    // console.log(event.change.target.files.file.name);
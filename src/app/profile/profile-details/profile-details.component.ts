import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ProfileService } from '../service/profile.service';
import { ProfileData } from '../profile-data/profiledata';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UploadProfileDataRequestModel } from '../profile-data/uploadprofiledata';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css'],
})
export class ProfileDetailsComponent implements OnInit, OnDestroy {
  // public profile;
  public newProfileData: ProfileData;
  public imagePath;
  public fileName: string;
  public contentType: string;
  public base64textString: string;
  public profilePic: string;
  public officialEmail: string;
  // bypassSecurityTrustResourceUrl(value: string) : SafeResourceUrl
  isLoading = false;
  subscription: Subscription;

  // profileData: ProfileData[];

  constructor(
    private profileService: ProfileService,
    private snackBar: MatSnackBar,
    private localStorage: LocalStorage,
    private router: Router,
    private toastr: ToastrService,
    // public _sanitizer: DomSanitizer
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.officialEmail = JSON.parse(localStorage.getItem('officialEmail'));
    this.onShowProfileData();
    this.getProfilePic();
  }

  //  this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
  //   + toReturnImage.base64string);

  onEditProfile(form: NgForm): void {
    const value = form.value;
    const nameArr = form.value.name.split(' ');
    if (nameArr.length === 1) {
      value.firstName = form.value.name;
      value.middleName = '';
      value.lastName = '';
    } else if (nameArr.length === 2) {
      value.firstName = nameArr[0];
      value.middleName = '';
      value.lastName = nameArr[1];
    } else if (nameArr.length >= 3) {
      value.firstName = nameArr[0];
      value.middleName = nameArr[1];
      value.lastName = nameArr[2];
    }
    console.log('========value data form:', value, nameArr);
    this.isLoading = true;
    this.subscription = this.profileService.updateProfileData(value).subscribe(
      (profileData) => {
        // callback
        this.isLoading = false;
        console.log('-------Response(Callback):', profileData);
        if (profileData.code === 200) {
          this.onShowProfileData();
        } else if (profileData.code === 400) {
          // Alert => profileData.message
          // alert(profileData.message);
          this.snackBar.open(profileData.message, 'Close', {
            duration: 1500,
          });
        }
      },
      (errorMessage) => {
        console.log('CORS Error', errorMessage);
        if (errorMessage.statusText == 'Unknown Error') {
          this.toastr.error(
            'Some unknown error occured. Please contact administrator'
          );
          this.isLoading = false;
        }
      }
    );
  }

  onShowProfileData(): void {
    this.isLoading = true;
    this.subscription = this.profileService.getProfileData().subscribe(
      (data) => {
        this.isLoading = false;
        console.log('---------Before Data:', this.newProfileData);
        if (data.code === 200) {
          this.newProfileData = data.userObj;
        } else if (data.code >= 400) {
          this.localStorage.clearAccessToken();
          this.router.navigate(['/login']);
          this.snackBar.open('Session Expired', 'close', {
            duration: 1500,
          });
        }
        this.newProfileData.name = `${data.userObj.firstName} ${data.userObj.middleName} ${data.userObj.lastName}`;
        console.log('---------After Data:', this.newProfileData);
      },
      (errorMessage) => {
        console.log('CORS Error', errorMessage);
        if (errorMessage.statusText == 'Unknown Error') {
          this.toastr.error(
            'Some unknown error occured. Please contact administrator'
          );
          this.isLoading = false;
        }
      }
    );
  }

  uploadProfilePic(): void {
    const uploadProfileData: UploadProfileDataRequestModel = new UploadProfileDataRequestModel();
    uploadProfileData.fileName = this.fileName;
    uploadProfileData.contentType = this.contentType;
    uploadProfileData.base64File = this.base64textString;
    this.isLoading = true;
    this.subscription = this.profileService
      .uploadProfilePic(uploadProfileData)
      .subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            this.getProfilePic();
            // this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + toReturnImage.base64textString);
            // this.getProjectFilesList();
            this.snackBar.open('Uploaded Successfully', 'Close', {
              duration: 1500,
            });
            this.fileName = null;
            this.contentType = null;
            this.base64textString = null;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onFileSelected(event): void {
    // console.log('====check event and id====', event, id);
    console.log(event.target.files[0]);
    const files = event.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this.handleFile.bind(this);
      reader.readAsBinaryString(file);
      this.fileName = file.name;
      this.contentType = file.type;
    }
  }

  handleFile(event) {
    var binaryString = event.target.result;
    this.base64textString = btoa(binaryString);
    this.uploadProfilePic();
    // console.log(this.base64textString);
  }

  getProfilePic(): void {
    this.isLoading = true;
    this.subscription = this.profileService
      .getProfilePic(this.officialEmail)
      .subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl(
              'data:image/jpg;base64,' + response.profilePic
            );
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  openSnackBar(): void {
    this.snackBar.open('Saved Successfully!', 'Close', {
      duration: 1200,
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

// ngOnInit(): void {
//   this.onRefresh();
// this.subscription = this.profileService.getProfileData()
// .subscribe(
//   data => {
//   // this.profile = data;
//   console.log('---------Before Data:', this.newProfileData);
//   this.newProfileData = data.userObj;
//   console.log('---------After Data:', this.newProfileData);
//   // console.log(this.profile);
// },
// error => {
//   console.log(error);
// });
// }

// getprofile(): void {
//   this.subscription = this.profileService.getProfileData()
//   .subscribe(data => {
//     // this.profileData = data
//     const newProfile = this.profile = data;
//     // newProfile = Array.of(this.profileData);
//     console.log(this.profile = data);
//     console.log(
//       'First Name:' + newProfile.userObj.firstName,
//       'Middle Name:' + newProfile.userObj.middleName,
//       'Last Name:' + newProfile.userObj.lastName,
//       'Email:' + newProfile.userObj.companyEmail,
//       'Phone Number:' + newProfile.userObj.phoneNumber,
//       'User Name:' + newProfile.userObj.userName
//     );
//   },
//   error => {
//     console.log(error);
//   });
// }

// onEditProfile(form: NgForm): void {
//   // console.log(form);
//   const value = form.value;
//   console.log('========value data form::', value);

//   this.newProfileData = new ProfileData();
//   console.log('========edit profile:', this.newProfileData);

//   this.subscription = this.profileService.updateProfileData(value).subscribe(profileData => {
//       // newProfileData = data;
//       console.log('---------Before Value:', value);
//       profileData.data = value;
//       // this.onRefresh();
//       this.subscription = this.profileService.getProfileData()
//       .subscribe(
//       data => {
//       // this.profile = data;
//       // console.log('---------Before Data:', this.newProfileData);
//       this.newProfileData = profileData.data;
//       console.log('---------After Data:', this.newProfileData);
//       // console.log(this.profile);
//   },
//   error => {
//     console.log(error);
//   });
//       console.log('---------After Value:', value);
//     });
//   }

//   onRefresh(): void {
//  this.subscription = this.profileService.getProfileData()
// .subscribe(
//   data => {
//   // this.profile = data;
//   console.log('---------Before Data:', this.newProfileData);
//   this.newProfileData = data.userObj;
//   console.log('---------After Data:', this.newProfileData);
//   // console.log(this.profile);
// },
// error => {
//   console.log(error);
// });
//   }

// const newProfileData = new ProfileData(
//   value.firstName,
//   // value.middleName,
//   // value.lastName,
//   value.phoneNumber,
//   value.personalEmailId,
//   value.companyEmail,
//   value.education,
//   value.description,
//   value.designAesthetics,
//   value.favColorCombo,
//   value.favDecorItem,
//   value.hobbiesAndInterests);
// console.log(newProfileData);

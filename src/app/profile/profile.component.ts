import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
// mySubscription: any;

  constructor(private router: Router) {
  //    // override the route reuse strategy
  //    this.router.routeReuseStrategy.shouldReuseRoute = () => {
  //     return false;
  //    }

  //    this.router.events.subscribe((evt) => {
  //     if (evt instanceof NavigationEnd) {
  //        // trick the Router into believing it's last link wasn't previously loaded
  //        this.router.navigated = false;
  //        // if you need to scroll back to top, here is the right place
  //        window.scrollTo(0, 0);
  //     }
  // });
  }

  ngOnInit(): void {
    // const currentUrl = this.router.url;
    this.router.navigate(['profile/profileDetails'], { skipLocationChange: true });
    // .then(() => {
    //   this.router.navigate([currentUrl]);
    // });

    // .then(() => {
    //   setTimeout(() => {
    //     window.location.reload();
    //   }, 1000);
    // });
  }

  // this.router.routeReuseStrategy.shouldReuseRoute = function () {
  //   return false;
  // };

  // this.mySubscription = this.router.events.subscribe((event) => {
  //   if (event instanceof NavigationEnd) {
  //     // Trick the Router into believing it's last link wasn't previously loaded
  //     this.router.navigated = false;
  //   }
  // });

  // ngOnDestroy(): void {
  //     if (this.mySubscription) {
  //     this.mySubscription.unsubscribe();
  //   }
  // }

}



//   navbarOpen = false;
//   toggleNavbar(): void {
//   this.navbarOpen = !this.navbarOpen;
// }

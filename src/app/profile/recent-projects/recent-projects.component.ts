import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recent-projects',
  templateUrl: './recent-projects.component.html',
  styleUrls: ['./recent-projects.component.css']
})
export class RecentProjectsComponent implements OnInit {

  selectedFile: File = null;
  isShown = false;

  constructor() { }

  ngOnInit(): void {
  }

  onFileSelected(event): void {
    console.log(event.target.files[0]);
    this.selectedFile = (event.target.files[0] as File);
    console.log('======Show Result data=====', this.selectedFile);
    this.selectedFile = (event.target.files[0].name as File);
    console.log('======Show Result name=====', this.selectedFile);
    // console.log(event.change.target.files.file.name);
  }

  onChange(): void {
    this.isShown = !this.isShown;
  }

}

export class UploadProfileDataRequestModel {
    public fileName: string;
    public contentType: string;
    public base64File: string;
}
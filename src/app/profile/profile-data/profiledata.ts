export class ProfileData {
  public name: string;
  public firstName: string;
  public middleName: string;
  public lastName: string;
  public phoneNumber: string;
  public email: string;
  public designCafeEmail: string;
  public education: string;
  public gender: number;
  public maritalStatus: number;
  public description: string;
  public designAesthetics: string;
  public favGoToColoursForProjects: string;
  public favDecorItem: string;
  public hobbiesAndInterests: string;
  public nameAsPerBankAcc: string;
  public ifscCode: string;
  public bankName: string;
  public panTanNumber: string;
  public accountNumber: string;
  public aadharNumber: string;
  public branchName: string;
  public gstNumber: string;
}

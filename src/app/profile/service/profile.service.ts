import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { ProfileData } from '../profile-data/profiledata';
import { UploadProfileDataRequestModel } from '../profile-data/uploadprofiledata';

@Injectable({providedIn: 'root'})
export class ProfileService {

    constructor(private http: HttpClient, private baseUrl: BaseUrl, private localStorage: LocalStorage) {}

    public getToken = this.localStorage.getAccessToken();
    public header = new HttpHeaders({'Authorization': this.getToken});

    getProfileData(): Observable<any> {
        // const getToken = localStorage.getItem('token');
        // const getToken = this.localStorage.getAccessToken();
        const param = new HttpParams().set('token', this.getToken);
        return this.http.get(this.baseUrl.getFinalUrl('auto-authenticate') , {params: param});
        // return this.http.get(this.baseUrl.baseURL + 'auto-authenticate', {params: param});
    }

    updateProfileData(profile: ProfileData): Observable<any> {
        // const getToken = localStorage.getItem('token');
        // const getToken = this.localStorage.getAccessToken();
        return this.http.put(this.baseUrl.getFinalUrl('user'), profile, {headers: this.header});
        // return this.http.put(this.baseUrl.baseURL + 'user', profile,
        // {
        //     headers: new HttpHeaders({ 'Authorization' : this.getToken })
        // }
    }

    uploadProfilePic(uploadProfileData: UploadProfileDataRequestModel) : Observable<any> {
        return this.http.post(this.baseUrl.getFinalUrl('upload-profile-pic'), uploadProfileData, {headers: this.header});
    }

    getProfilePic(designCafeEmail: string): Observable<any> {
        const param = new HttpParams().set('designCafeEmail', designCafeEmail);
        return this.http.get(this.baseUrl.getFinalUrl('profile-pic'), {params: param, headers: this.header});
    }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LeadComponent } from './lead/lead.component';
import { LoginComponent } from './login/login.component';
import { BankAccountDetailsComponent } from './profile/bank-account-details/bank-account-details.component';
import { ProfileDetailsComponent } from './profile/profile-details/profile-details.component';
import { ProfileComponent } from './profile/profile.component';
import { RecentProjectsComponent } from './profile/recent-projects/recent-projects.component';
import { FilesComponent } from './project/files/files.component';
import { MomComponent } from './project/mom/mom.component';
import { PaymentsComponent } from './project/payments/payments.component';
import { ProjectDetailsComponent } from './project/project-details/project-details.component';
import { ProjectLinksComponent } from './project/project-links/project-links.component';
import { ProjectMilestonesComponent } from './project/project-milestones/project-milestones.component';
import { ProjectComponent } from './project/project.component';

const routes: Routes = [
  // { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'lead', component: LeadComponent },
  { path: 'project',
    component: ProjectComponent,
    children: [
      {
        path: 'projectLinks',
        component: ProjectLinksComponent,
        children: [
          { path: 'projectDetails', component: ProjectDetailsComponent },
          { path: 'projectMilestones', component: ProjectMilestonesComponent },
          { path: 'files', component: FilesComponent },
          { path: 'payments', component: PaymentsComponent },
          { path: 'mom', component: MomComponent }
        ]
      }
    ],
  },
  {
    path: 'profile',
    component: ProfileComponent,
    children: [
      { path: 'profileDetails', component: ProfileDetailsComponent },
      { path: 'bankAccountDetails', component: BankAccountDetailsComponent },
      { path: 'recentProjects', component: RecentProjectsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

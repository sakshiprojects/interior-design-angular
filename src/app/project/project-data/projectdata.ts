export class ProjectData {
    public _id: string;
    public name: string;
    public designName: string;
    public currentMilestone: string;
    public valueINR: string;
    public achievedRevenue: string;
    public status: string;
    public plannedHandover: string;
}

export class UpdateTeamMemberRequestModel {
    public id: string;
    public designer: string;
    public salesExecutive: string;
    public threeDDesignRender: string;
    public threeDDesignShell: string;
    public chmExecutive: string;
    public surveyExecutive: string;
}
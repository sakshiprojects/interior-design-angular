import { MomDataAttachmentFileRequestModel } from './momdataattachmentfile';

export class MomDataRequestModel {
    public meetingAgenda: number;
    public meetingDate: string;
    public descriptionText: string;
    public projectID: string;
    public id: string;
    public attachment: MomDataAttachmentFileRequestModel;
}

export class MomListDataRequestModel {
    public isSent: boolean;
    public status: string;
    public momID: string;
    public descriptionText: string;
    public meetingAgenda: string;
    public meetingDate: string;
    public attachment: string;
    public awsAttachmentKey: string;
}

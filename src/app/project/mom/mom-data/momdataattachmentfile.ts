export class MomDataAttachmentFileRequestModel {
    public fileName: string;
    public contentType: string;
    public fileBase64: string;
}
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { MomDataRequestModel } from '../mom-data/momdata';

@Injectable({providedIn: 'root'})
export class MomService {
    constructor(private http: HttpClient, private baseUrl: BaseUrl, private localStorage: LocalStorage) {}

    public getToken = this.localStorage.getAccessToken();

    onAddMom(momData: MomDataRequestModel): Observable<any> {
        return this.http.post(this.baseUrl.getFinalUrl('addMOM'), momData,
        {
            headers: new HttpHeaders({ 'Authorization': this.getToken })
        }
        );
    }

    onUpdateMom(momData: MomDataRequestModel): Observable<any> {
        return this.http.put(this.baseUrl.getFinalUrl('updateMOM'), momData,
        {
            headers: new HttpHeaders({ 'Authorization': this.getToken })
        }
        );
    }

    getMomList(id: string): Observable<any> {
        const header = new HttpHeaders({ 'Authorization': this.getToken });
        const param = new HttpParams().set('projectID', id);
        return this.http.get(this.baseUrl.getFinalUrl('getMOMList'), {headers: header, params: param});
    }
}

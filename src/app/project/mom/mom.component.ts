import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { ProjectDetailsData } from '../project-data/project-details-data';
import { ProjectService } from '../project-service/project.service';
import { MomDataRequestModel } from './mom-data/momdata';
import { MomListDataRequestModel } from './mom-data/momlistdata';
import { MomService } from './mom-service/mom.service';
import { ToastrService } from 'ngx-toastr';
import { MomDataAttachmentFileRequestModel } from './mom-data/momdataattachmentfile';

@Component({
  selector: 'app-mom',
  templateUrl: './mom.component.html',
  styleUrls: ['./mom.component.css'],
})
export class MomComponent implements OnInit, OnDestroy {
  public projectMomListArr = [];
  public momListItem;
  public projectMomItem;
  public newMomItem;
  public newID;
  public updatedDate = moment().format('DD/MM/YYYY');
  public isShown = false;
  public isClick = false;
  public isLoading = false;
  public selectedMeetingAgenda = 'KYC';
  subscription: Subscription;
  public meetingAgendaArray = [
    'KYC',
    'First Cut Meeting',
    'Design Iteration',
    'Design Finalization',
    '3D Presentation',
    'Design Revision (3D)',
    'Design Sign Off',
  ];
  public description = '';
  public momID: string = null;
  public selectedFile: File = null;
  base64textString: string = null;
  fileName: string = null;
  contentType: string = null;
  attachmentURL: string = null;
  attachmentDisplayName: string = null;
  // @ViewChild('editor') editor;

  date = new FormControl(new Date());

  constructor(
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
    private momService: MomService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    console.log(
      '---------------------------MOM COmponent on Init--------------------------'
    );
    this.projectMomItem = JSON.parse(localStorage.getItem('projectData'));
    this.newID = this.projectMomItem._id;
    this.onGetProjectDetails();
    this.onGetMomList();
    this.getMOMData();
  }

  getMOMData(): void {
    const momData: MomListDataRequestModel = JSON.parse(
      localStorage.getItem('momData')
    );
    if (momData) {
      if (momData.descriptionText) {
        this.description = momData.descriptionText;
      }
      if (momData.meetingAgenda) {
        this.selectedMeetingAgenda = momData.meetingAgenda;
      }
      if (momData.meetingDate) {
        console.log(
          'MOMENT',
          moment(momData.meetingDate, 'DD/MM/YYYY').toDate()
        );
        console.log('Using Date', new Date());
        this.date = new FormControl(
          moment(momData.meetingDate, 'DD/MM/YYYY').toDate()
        );
      }
      if (momData.momID) {
        this.momID = momData.momID;
      } else {
        this.momID = null;
      }
      if (momData.attachment && momData.awsAttachmentKey) {
        this.attachmentURL = momData.attachment;
        this.attachmentDisplayName = momData.awsAttachmentKey;
      } else {
        this.attachmentURL = null;
        this.attachmentDisplayName = null;
      }
    }
  }

  onSwitch(): void {
    localStorage.removeItem('momData');
    this.isShown = !this.isShown;
    this.momID = null;
    this.selectedMeetingAgenda = 'KYC';
    this.date = new FormControl(new Date());
    this.description = '';
    this.attachmentURL = null;
    this.attachmentDisplayName = null;
    // this.saveMom();
    this.onGetMomList();
  }

  onClick(item): void {
    localStorage.setItem('momData', JSON.stringify(item));
    this.isShown = !this.isShown;
    console.log(
      '---------------------------MOM COmponent on Init--------------------------'
    );
    this.projectMomItem = JSON.parse(localStorage.getItem('projectData'));
    this.newID = this.projectMomItem._id;
    this.onGetProjectDetails();
    this.onGetMomList();
    this.getMOMData();
  }

  goToUrl(url): void {
    if (url) {
      // this.document.location.href = url;
      window.open(url)
    }
}

  onGetProjectDetails(): void {
    this.isLoading = true;
    this.subscription = this.projectService
      .getProjectDetails(this.newID)
      .subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            const projectDetailsData: ProjectDetailsData = new ProjectDetailsData();
            if (response.data) {
              projectDetailsData.name = response.data.projectName;
              if (response.data.customer) {
                if (response.data.customer.name) {
                  projectDetailsData.customerName = response.data.customer.name;
                }
                if (response.data.customer.phoneNumber) {
                  projectDetailsData.contactNo =
                    response.data.customer.phoneNumber;
                }
              }
              if (response.data.designer && response.data.designer.firstName) {
                projectDetailsData.designer =
                  response.data.designer.firstName +
                  (response.data.designer.middleName
                    ? ` ${response.data.designer.middleName} `
                    : '') +
                  (response.data.designer.lastName
                    ? ` ${response.data.designer.lastName} `
                    : '');
              }
            }
            this.newMomItem = projectDetailsData;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onGetMomList(): void {
    this.isLoading = true;
    this.subscription = this.momService.getMomList(this.newID).subscribe(
      (response) => {
        this.isLoading = false;
        if (response.code === 200) {
          const projectMomList: MomListDataRequestModel[] = [];
          if (response.data && response.data.MOM) {
            response.data.MOM.forEach((element) => {
              const momList: MomListDataRequestModel = new MomListDataRequestModel();
              momList.isSent = element.isSent;
              momList.status = this.getMomStatus(element.status);
              momList.momID = element._id;
              momList.descriptionText = element.descriptionText;
              momList.meetingAgenda = this.getMeetingAgendaTitleFromCode(
                element.meetingAgenda
              );
              momList.meetingDate = element.meetingDate;
              momList.attachment = element.attachment;
              momList.awsAttachmentKey = element.awsAttachmentKey;
              projectMomList.push(momList);
            });
          }
          this.projectMomListArr = projectMomList;
        } else {
          this.snackBar.open(response.message, 'close', {
            duration: 1500,
          });
        }
      },
      (errorMessage) => {
        console.log('CORS Error', errorMessage);
        if (errorMessage.statusText == 'Unknown Error') {
          this.toastr.error(
            'Some unknown error occured. Please contact administrator'
          );
          this.isLoading = false;
        }
      }
    );
  }

  saveMom(): void {
    if (this.momID) {
      // Update API
      const requestMomData: MomDataRequestModel = new MomDataRequestModel();
      requestMomData.meetingDate = this.updatedDate;
      requestMomData.id = this.momID;
      requestMomData.descriptionText = this.description;
      requestMomData.meetingAgenda = this.getMomMeetingAgenda(this.selectedMeetingAgenda);
      if (this.fileName && this.contentType && this.base64textString) {
        // console.log('======after Check filename, content and base4', this.fileName, this.contentType, this.base64textString);
        const requestAttachmentFile: MomDataAttachmentFileRequestModel = new MomDataAttachmentFileRequestModel();
        requestAttachmentFile.fileName = this.fileName;
        requestAttachmentFile.contentType = this.contentType;
        requestAttachmentFile.fileBase64 = this.base64textString;
        requestMomData.attachment = requestAttachmentFile;
        // console.log('======attachment model=====', requestMomData.attachment, requestAttachmentFile);
      }
      this.isLoading = true;
      this.subscription = this.momService.onUpdateMom(requestMomData).subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            this.snackBar.open('Updated Successfully', 'Close', {
              duration: 1500,
            });
            this.attachmentURL = response.data.attachment;
            this.attachmentDisplayName = response.data.awsAttachmentKey;
            // this.onGetMomList();
            // this.selectedMeetingAgenda = 'KYC';
            // this.date = new FormControl(new Date());
            // this.description = '';
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
    } else {
      // Add API
      const requestMomData: MomDataRequestModel = new MomDataRequestModel();
      requestMomData.meetingDate = this.updatedDate;
      requestMomData.projectID = this.newID;
      requestMomData.descriptionText = this.description;
      requestMomData.meetingAgenda = this.getMomMeetingAgenda(this.selectedMeetingAgenda);
      // console.log('======check filename, content and base4', this.fileName, this.contentType, this.base64textString);
      
      if (this.fileName && this.contentType && this.base64textString) {
        // console.log('======after Check filename, content and base4', this.fileName, this.contentType, this.base64textString);
        const requestAttachmentFile: MomDataAttachmentFileRequestModel = new MomDataAttachmentFileRequestModel();
        requestAttachmentFile.fileName = this.fileName;
        requestAttachmentFile.contentType = this.contentType;
        requestAttachmentFile.fileBase64 = this.base64textString;
        requestMomData.attachment = requestAttachmentFile;
        // console.log('======attachment model=====', requestMomData.attachment, requestAttachmentFile);
      }
      console.log('=====Request:', requestMomData);
      this.isLoading = true;
      this.subscription = this.momService.onAddMom(requestMomData).subscribe(response => {
          console.log('=====response:', response);
          this.isLoading = false;
          if (response.code === 200) {
            if (response.data && response.data._id) {
              const momID = response.data._id;
              this.attachmentURL = response.data.attachment;
              this.attachmentDisplayName = response.data.awsAttachmentKey;
              this.momID = momID;
              this.snackBar.open('Saved Successfully', 'Close', {
                duration: 1500,
              });
              this.fileName = null;
              this.contentType = null;
              this.base64textString = null;
              // this.onGetMomList();
              //   this.selectedMeetingAgenda = 'KYC';
              //   this.date = new FormControl(new Date());
              //   this.description = '';
            }
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
    }
  }

  onFileSelected(event): void {
    console.log(event.target.files[0]);
    const files = event.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this.handleFile.bind(this);
      reader.readAsBinaryString(file);
      this.fileName = file.name;
      this.contentType = file.type;
      this.attachmentDisplayName = file.name;
    }
  }

  handleFile(event) {
    var binaryString = event.target.result;
    this.base64textString = btoa(binaryString);
    // console.log(this.base64textString);
  }

  addEvent(event: MatDatepickerInputEvent<Date>): void {
    const selectedDate = moment(event.value).format('DD/MM/YYYY');
    this.updatedDate = selectedDate;
  }

  getMomMeetingAgenda(meetingAgenda: string): number {
    switch (meetingAgenda) {
      case 'KYC':
        return 0;
      case 'First Cut Meeting':
        return 1;
      case 'Design Iteration':
        return 2;
      case 'Design Finalization':
        return 3;
      case '3D Presentation':
        return 4;
      case 'Design Revision (3D)':
        return 5;
      case 'Design Sign Off':
        return 6;
    }
  }

  getMeetingAgendaTitleFromCode(agendaEnumCode: number): string {
    switch (agendaEnumCode) {
      case 0:
        return 'KYC';
      case 1:
        return 'First Cut Meeting';
      case 2:
        return 'Design Iteration';
      case 3:
        return 'Design Finalization';
      case 4:
        return '3D Presentation';
      case 5:
        return 'Design Revision (3D)';
      case 6:
        return 'Design Sign Off';
      default:
        return '-';
    }
  }

  getMomStatus(status: number): string {
    switch (status) {
      case 1:
        return 'DRAFT';
      case 2:
        return 'SHARED';
      default:
        return '-';
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    console.log('======= check on destroy======');
  }
}

// setStyle(style: string) {
//   let bool = document.execCommand(style, false, null);
// }

// onChange(): void {
//   console.log(this.editor.nativeElement['innerHTML']);
// }

// this.selectedFile = (event.target.files[0] as File);
// console.log('======Show Result data=====', this.selectedFile);
// this.selectedFile = (event.target.files[0].name as File);
// console.log('======Show Result name=====', this.selectedFile);
// console.log(event.change.target.files.file.name);

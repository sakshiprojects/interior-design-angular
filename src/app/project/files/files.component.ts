import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FilesDataListRequestModel } from './files-data/filesdatalist';
import { FilesService } from './files-service/files.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css'],
})
export class FilesComponent implements OnInit, OnDestroy {
  public projectFilesListArr: FilesDataListRequestModel[] = [];
  public projectItems;
  public id;
  public fileName: string = null;
  public currentEditingFileID: string = null;
  public contentType: string = null;
  public base64textString: string = null;
  public fileURL: string = null;
  public isLoading = false;
  public subscription: Subscription;
  public apiResponse: FilesDataListRequestModel[] = [];
  public phase = 0;

  constructor(
    private filesService: FilesService,
    private snackBar: MatSnackBar,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.projectItems = JSON.parse(localStorage.getItem('projectData'));
    this.id = this.projectItems._id;
    this.getProjectFilesList();
    // this.router.navigate(['project/projectLinks/files/preDesign'], { skipLocationChange: true });
  }

  goToUrl(url): void {
    // console.log('=====download check====', url,i,id);
    if (url) {
      // this.document.location.href = url;
      window.open(url);
    }
  }

  getProjectFilesList(): void {
    this.currentEditingFileID = null;
    this.isLoading = true;
    this.subscription = this.filesService
      .getProjectFilesList(this.id)
      .subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            const projectFilesArr: FilesDataListRequestModel[] = [];
            if (response.data && response.data.projectFiles) {
              response.data.projectFiles.forEach((element) => {
                const filesList: FilesDataListRequestModel = new FilesDataListRequestModel();
                filesList._id = element._id;
                filesList.displayName = element.displayName;
                filesList.projectPhaseStatus = element.projectPhaseStatus;
                filesList.fileURL = element.fileURL;
                filesList.uploadDateTime = element.uploadDateTime;
                if (element.uploadedBy && element.uploadedBy.firstName) {
                  filesList.uploadedBy =
                  element.uploadedBy.firstName +
                  (element.uploadedBy.middleName ? ` ${element.uploadedBy.middleName} ` : '') +
                  (element.uploadedBy.lastName ? ` ${element.uploadedBy.lastName} ` : '');
                }
                projectFilesArr.push(filesList);
              });
            }
            //  this.projectFilesListArr = projectFilesArr;
            this.apiResponse = projectFilesArr;
            this.segregateMilestoneForPhases(this.phase);
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  segregateMilestoneForPhases(phase): void {
    this.phase = phase;
    const projectFilesArr: FilesDataListRequestModel[] = [];
    this.apiResponse.forEach((element) => {
      if (element.projectPhaseStatus === phase) {
        projectFilesArr.push(element);
      }
    });
    this.projectFilesListArr = projectFilesArr;
  }

  updateProjectFile(): void {
    const requestAttachmentFile: FilesDataListRequestModel = new FilesDataListRequestModel();
    // if (id && this.fileName && this.contentType && this.base64textString) {
    // console.log(
    //   '====check filename, contenttype, base64 and requestattachfile object===',
      
    //   this.fileName,
    //   this.contentType,
    //   this.base64textString
    // );
    // console.log('======after Check filename, content and base4', this.fileName, this.contentType, this.base64textString)
    requestAttachmentFile.id = this.currentEditingFileID;
    requestAttachmentFile.fileName = this.fileName;
    requestAttachmentFile.contentType = this.contentType;
    requestAttachmentFile.fileBase64 = this.base64textString;
    this.isLoading = true;
    // requestpaymentConfirmationData.attachment = requestAttachmentFile;
    // console.log('======attachment model=====', requestMomData.attachment, requestAttachmentFile);
    // }
    // console.log('=======requestattachfile data=======', requestAttachmentFile);

    this.subscription = this.filesService
      .updateProjectFile(requestAttachmentFile)
      .subscribe(
        (response) => {
          this.isLoading = false;
          // console.log(
          //   '=======requestattachfile data after api hit=======',
          //   requestAttachmentFile
          // );
          if (response.code === 200) {
            // console.log(
            //   '=======requestattachfile data after success response=======',
            //   requestAttachmentFile,
            //   response
            // );
            this.getProjectFilesList();
            this.snackBar.open('Added Successfully', 'Close', {
              duration: 1500,
            });
            // this.fileID = null;
            this.fileName = null;
            this.contentType = null;
            this.base64textString = null;
            this.currentEditingFileID = null;
            //  this.supportingDocumentURL = response.data.supportingDocumentURL;
            //  this.awsSupportingDocumentURLKey = response.data.awsSupportingDocumentURLKey;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onFileSelected(event, id): void {
    // console.log('====check event and id====', event, id);
    console.log(event.target.files[0]);
    const files = event.target.files;
    var file = files[0];
    if (files && file) {
      console.log('====check event and id after file====', id);
      this.currentEditingFileID = id
      var reader = new FileReader();
      reader.onload = this.handleFile.bind(this);
      
      reader.readAsBinaryString(file);
      

      this.fileName = file.name;
      this.contentType = file.type;

    }
  }

  handleFile(event) {
    var binaryString = event.target.result;
    this.base64textString = btoa(binaryString);
    this.updateProjectFile();
    console.log(
      '==========================================Generated base 64------------------------------------', this.currentEditingFileID , this.projectFilesListArr
    );

    // console.log(this.base64textString);
  }

  // checkID(id): void {
  //   console.log('=====checking ID======', id);
    
  // }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

// localStorage.setItem('fileID', JSON.stringify(element._id));

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { FilesDataListRequestModel } from '../files-data/filesdatalist';

@Injectable({providedIn: 'root'})
export class FilesService {
    constructor(private http: HttpClient, private baseUrl: BaseUrl, private localStorage: LocalStorage) {}

    public getToken = this.localStorage.getAccessToken();
    public header = new HttpHeaders({'Authorization': this.getToken});

    getProjectFilesList(id: string): Observable<any> {
        // const header = new HttpHeaders({'Authorization': this.getToken});
        const param = new HttpParams().set('projectID', id);
        return this.http.get(this.baseUrl.getFinalUrl('getProjectFilesList'), {headers: this.header, params: param});
    }

    updateProjectFile(requestFileData: FilesDataListRequestModel): Observable<any> {
        return this.http.post(this.baseUrl.getFinalUrl('updateProjectFile'), requestFileData, {headers: this.header});
    }
}
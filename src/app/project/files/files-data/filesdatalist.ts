export class FilesDataListRequestModel {
    public _id: string;
    public id: string;
    public displayName: string;
    public projectPhaseStatus: number;
    public fileBase64: string
    public fileName: string
    public contentType: string;
    public fileURL: string;
    public uploadDateTime: string;
    public uploadedBy: string;
}
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ProjectData } from './project-data/projectdata';
import { ProjectService } from './project-service/project.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, OnDestroy {
  projectItemArr = [];
  subscription: Subscription;
  isLoading = false;
  // error: string = null;

  constructor(
    private router: Router,
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.ongetProjectList();
  }

  onProjectLink(item): void {
    localStorage.setItem('projectData', JSON.stringify(item));
    // console.log('<=====Check Items=====>', item);
    this.router.navigate(['project/projectLinks'], { skipLocationChange: true });
  }

  ongetProjectList(): void {
    this.isLoading = true;
    this.subscription = this.projectService.getProjectList().subscribe(response => {
      this.isLoading = false;
      if (response.code === 200) {
        const projectsArr: ProjectData[] = [];
        // this.projectItemArr = response.data.projects.data;
        if (response.data && response.data.projects && response.data.projects.data) {
          // console.log('=====Response check======', response.data.projects.data.length);
          response.data.projects.data.forEach(element => {     // here element is item at the perticular index
            // console.log('=====Element check======', element.projectName);
            const project: ProjectData = new ProjectData();
            project._id = element._id;
            project.name = element.projectName;
            if (element.designer && element.designer.firstName) {
              project.designName =
              element.designer.firstName +
              (element.designer.middleName ? ` ${element.designer.middleName}` : '') +
              (element.designer.lastName ? ` ${element.designer.lastName}` : '');
            }
            project.currentMilestone = this.getProjectMilestone(element.currentMilestone);
            // project.currentMilestone = element.currentMilestone;
            project.valueINR = element.totalProjectValue;
            project.achievedRevenue = element.achievedRevenueValue;
            project.status = this.getProjectStatusName(element.projectStatus);
            project.plannedHandover = element.plannedHandoverDate;
            projectsArr.push(project);
          });
        }
        this.projectItemArr = projectsArr;
      } else {
        this.snackBar.open(response.message, 'close', {
          duration: 1500,
        });
      }
    },
    errorMessage => {
      console.log("CORS Error",errorMessage);
      if(errorMessage.statusText == "Unknown Error"){
      // this.error = errorMessage;
      this.toastr.error('Some unknown error occured. Please contact administrator');
      // alert('BE Stopped Working...');
      // this.showErrorAlert(errorMessage);
      this.isLoading = false
    }
    }
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getProjectStatusName(status: number): string {
    switch (status) {
      case 1:
        return 'Active';
      case 2:
        return 'HOLD';
      case 3:
        return 'WITHDRAWN';
      case 4:
        return 'COMPLETED';
      default:
        return '-';
    }
  }

  getProjectMilestone(milestone: string): string {
    switch (milestone) {
      case '0':
        return 'Project Signup';
      case '1':
        return 'Site Survey';
      case '2':
        return 'Create 3D Shell';
      case '3':
        return 'Know Your Client';
      case '4':
        return 'First Cut Meeting';
      case '5':
        return 'Design Iteration';
      case '6':
        return 'Design Finalization';
      case '7':
        return 'Build 3D Design';
      case '8':
        return '3D Presentation';
      case '9':
        return 'Design Revision';
      case '10':
        return 'Prepare GFC (Production Drawings)';
      case '11':
        return 'Kickoff Meeting';
      case '12':
        return 'Design Revision (PM Checklist)';
      case '13':
        return 'Studio Manager - Checklist';
      case '14':
        return 'Design Sign-Off';
      case '15':
        return 'Selection Visit';
      case '16':
          return 'Production Started';
      case '17':
        return 'Factory Dispatch';
      case '18':
        return 'Handover';
      default:
        return '-';
    }
  }
}





//   onChangePage(pageOfItems: Array<any>): void {
//     // update current page of items
//     this.pageOfItems = pageOfItems;
// }


// this.items = Array(150).fill(0).map((x, i) => ({ id: (i + 1), name: `Item ${i + 1}`}));























  // projectItems2: string[] = ['Two', 'Two', 'Two', 'Two', 'Two', 'Two', 'Two'];
  // projectItems3: string[] = ['Three', 'Three', 'Three', 'Three', 'Three', 'Three', 'Three'];
  // projectItems4: string[] = ['Four', 'Four', 'Four', 'Four', 'Four', 'Four', 'Four'];
  // projectItems5: string[] = ['Five', 'Five', 'Five', 'Five', 'Five', 'Five', 'Five'];
  // projectItems6: string[] = ['Six', 'Six', 'Six', 'Six', 'Six', 'Six', 'Six'];
  // projectItems7: string[] = ['Seven', 'Seven', 'Seven', 'Seven', 'Seven', 'Seven', 'Seven'];

   // projectItems3 = {
  //   name: 'Test Design',
  //   designName: 'Decor',
  //   currentMilestone: 'Milestone123',
  //   valueINR: '456',
  //   achievedRevenue: '1000',
  //   status: 'Test Status',
  //   plannedHandover: 'planned test'
  // };

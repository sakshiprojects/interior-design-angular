import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-links',
  templateUrl: './project-links.component.html',
  styleUrls: ['./project-links.component.css']
  // inputs: [`showItem`]
})
export class ProjectLinksComponent implements OnInit {
  // @Input() showItem: any;
  // clickedItem: any;

  constructor(private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.router.navigate(['project/projectLinks/projectDetails'], { skipLocationChange: true });
    // console.log('====>child call<====', this.showItem);
    // this.clickedItem = this.showItem;
    // console.log('====== calling child component=====', this.clickedItem);
  }

  // onClickMilestone(): void {
  //   this.router.navigate(['project/projectLinks/projectMilestones/preDesignStage'], {skipLocationChange: true});
  //   this.location.replaceState('/project');
  // }

  // onClickFiles(): void {
  //   this.router.navigate(['project/projectLinks/files/preDesign'], {skipLocationChange: true});
  //   this.location.replaceState('/project');
  // }

}

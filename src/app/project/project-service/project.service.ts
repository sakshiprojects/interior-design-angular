import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { ProjectDetailsData } from '../project-data/project-details-data';
import { UpdateTeamMemberRequestModel } from '../project-data/updateteammembersdata';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  constructor(
    private http: HttpClient,
    private baseUrl: BaseUrl,
    private localStorage: LocalStorage
  ) {}
  public getToken = this.localStorage.getAccessToken();
  public header = new HttpHeaders({ Authorization: this.getToken });

  getProjectList(): Observable<any> {
    // const getToken = this.localStorage.getAccessToken();
    return this.http.get(this.baseUrl.getFinalUrl('getProjectList'), {
      headers: this.header,
    });
  }

  getProjectDetails(id: string): Observable<any> {
    // const id = projectID;
    // const getToken = this.localStorage.getAccessToken();
    // const header = new HttpHeaders({ 'Authorization' : this.getToken });
    const param = new HttpParams().set('id', id);
    return this.http.get(this.baseUrl.getFinalUrl('getProjectDetail'), {
      params: param,
      headers: this.header,
    });
  }

  updateProjectCommercial(
    projectDetailsData: ProjectDetailsData
  ): Observable<any> {
    return this.http.put(
      this.baseUrl.getFinalUrl('updateProjectCommercial'),
      projectDetailsData,
      { headers: this.header }
    );
  }

  updateTeamMember(
    updateTeamMemberData: UpdateTeamMemberRequestModel
  ): Observable<any> {
    return this.http.put(
      this.baseUrl.getFinalUrl('updateTeamMember'),
      updateTeamMemberData,
      { headers: this.header }
    );
  }

  findUserApiUseCase(key: string): Observable<any> {
    const param = new HttpParams().set('key', key);
    return this.http.get(this.baseUrl.getFinalUrl('find'), {
      params: param,
      headers: this.header,
    });
  }
}

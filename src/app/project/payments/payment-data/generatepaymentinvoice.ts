export class GeneratePaymentInvoiceRequestModel {
    public milestoneID: string;
    public projectID: string;
    public isRegenerate: boolean;
}
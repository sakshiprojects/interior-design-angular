import { AddPaymentAttachmentFileRequestModel } from './addpaymentattachmentfile';

export class AddPaymentConfirmationDataModel {
    public paymentName: string;
    public receivedAmount: string;
    public modeOfPayment: number;
    public transactionRefNo: string;
    public receivedDate: string;
    public paymentMilestoneObjectID: string;
    public projectID: string;
    public attachment: AddPaymentAttachmentFileRequestModel;
}
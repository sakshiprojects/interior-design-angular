export class AddPaymentAttachmentFileRequestModel {
    public fileName: string;
    public contentType: string;
    public fileBase64: string;
}

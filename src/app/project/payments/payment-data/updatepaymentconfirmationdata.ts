import { UpdatePaymentAttachmentFileRequestModel } from './updatepaymentattachmentfile';

export class UpdatePaymentConfirmationDataModel {
    public paymentName: string;
    public receivedAmount: string;
    public modeOfPayment: number;
    public transactionRefNo: string;
    public receivedDate: string;
    public paymentMilestoneObjectID: string;
    public projectID: string;
    public paymentConfirmationObjectID: string;
    public attachment: UpdatePaymentAttachmentFileRequestModel;
}

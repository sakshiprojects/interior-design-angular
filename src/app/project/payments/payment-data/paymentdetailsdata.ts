export class PaymentDetailsDataModel {
    public milestoneAmount: string;
    public advanceAmount: string;
    public addonAmount: string;
    public totalPayableAmount: string;
    public amountPaid: string;
    public amountDue: string;
    public isFreezed: boolean;
    public invoiceBaseAmount: string;
    public invoiceNumber: string;
    public invoiceTotalAmount: string;
    public invoiceTotalTaxAmount: string;
    public invoiceDate: string;
    public invoiceURL: string;
    public paymentLink: string;
}

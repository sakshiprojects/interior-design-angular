export class PaymentConfirmationDataModel {
    public _id: string;
    public paymentName: string;
    public receivedAmount: string;
    public modeOfPayment: string;
    public receivedDate: string;
    public transactionRefNo: string;
    public supportingDocumentURL: string;
    public awsSupportingDocumentURLKey: string;
}
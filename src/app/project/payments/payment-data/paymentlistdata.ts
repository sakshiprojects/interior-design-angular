export class PaymentListDataModel {
    public _id: string;
     public milestoneName: string;
     public dueDate: string;
     public paymentValue: string;
     public totalPayableAmount: string;
     public amountPaid: string;
     public amountDue: string;
     public status: string;
}
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { retryWhen } from 'rxjs/operators';
import { ProjectDetailsData } from '../project-data/project-details-data';
import { ProjectService } from '../project-service/project.service';
import { AddPaymentConfirmationDataModel } from './payment-data/addpaymentconfirmationdata';
import { GeneratePaymentInvoiceRequestModel } from './payment-data/generatepaymentinvoice';
// import { GeneratePaymentInvoiceListRequestModel } from './payment-data/generatepaymentinvoicelist';
import { PaymentConfirmationDataModel } from './payment-data/paymentconfirmationdata';
import { PaymentDetailsDataModel } from './payment-data/paymentdetailsdata';
import { PaymentListDataModel } from './payment-data/paymentlistdata';
import { UpdatePaymentConfirmationDataModel } from './payment-data/updatepaymentconfirmationdata';
import { PaymentService } from './payment-service/payment.service';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
import { AddPaymentAttachmentFileRequestModel } from './payment-data/addpaymentattachmentfile';
import { UpdatePaymentAttachmentFileRequestModel } from './payment-data/updatepaymentattachmentfile';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css'],
})
export class PaymentsComponent implements OnInit, OnDestroy {
  public isShown = false;
  public isClick = false;
  public isSwitch = false;
  public isAddMode = false;
  public isLoading = false;
  public projectPaymentListArr = [];
  public mainPaymentConfirmationArr = [];
  public updatedDate = moment().format('DD/MM/YYYY');
  public selectedPaymentMode = 'CHEQUE';
  public paymentModeArray = ['CHEQUE', 'CASH', 'IMPS', 'NEFT', 'ONLINE'];
  public currentEditingPayment = null;
  public isRegenerate = false;
  public projectData;
  public paymentData;
  public newData;
  public newID;
  public paymentID;
  public paymentDetailsItem;
  public recievedAmount: string;
  public transactionRefNo: string;
  public updatedRecievedAmount: string;
  public updatedTransactionRefNo: string;
  public projectModularValue: string;
  public addOnValue: string;
  public selectedFile: File = null;
  public base64textString: string = null;
  public fileName: string = null;
  public contentType: string = null;
  public supportingDocumentURL: string = null;
  public awsSupportingDocumentURLKey: string = null;
  public subscription: Subscription;

  date = new FormControl(new Date());

  constructor(
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
    private paymentService: PaymentService,
    private toastr: ToastrService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {
    this.projectData = JSON.parse(localStorage.getItem('projectData'));
    this.newID = this.projectData._id;
    this.onGetProjectDetails();
    this.onGetPaymentMilestoneList();
    // this.onGetPaymentMilestoneDetails();
  }

  goToUrl(url): void {
    if (url) {
      // this.document.location.href = url;
      window.open(url);
    }
  }

  onChange(modulerValue, addOnValue): void {
    this.isShown = !this.isShown;
    this.projectModularValue = modulerValue;
    this.addOnValue = addOnValue;
  }

  onSwitch(item): void {
    this.isClick = !this.isClick;
    localStorage.setItem('paymentData', JSON.stringify(item));
    this.paymentData = JSON.parse(localStorage.getItem('paymentData'));
    this.paymentID = this.paymentData._id;
    // this.isSwitch = false;
    // this.isLoading = true;
    this.onGetProjectDetails();
    // this.onGetPaymentMilestoneList();
    this.onGetPaymentMilestoneDetails();
  }

  onBack(): void {
    this.isClick = !this.isClick;
    this.onGetProjectDetails();
    this.onGetPaymentMilestoneList();
    this.onGetPaymentMilestoneDetails();
    this.isAddMode = false;
    this.recievedAmount = null;
    this.date = new FormControl(new Date());
    this.currentEditingPayment = null;
    this.selectedPaymentMode = 'CHEQUE';
    this.transactionRefNo = null;
  }

  onShow(
    id,
    receivedDate,
    modeOfPayment,
    updatedAmount,
    updatedRefNo,
    supportingDocument
  ): void {
    this.currentEditingPayment = id;
    if (id) {
      this.isAddMode = false;
      this.date = new FormControl(moment(receivedDate, 'DD/MM/YYYY').toDate());
      this.selectedPaymentMode = modeOfPayment;
      this.updatedRecievedAmount = updatedAmount;
      this.updatedTransactionRefNo = updatedRefNo;
      this.awsSupportingDocumentURLKey = supportingDocument;
      // this.awsSupportingDocumentURLKey = attachmentUrlDisplayName;
      console.log(
        '=====check editable value(updatedAmount)======',
        updatedAmount
      );
      console.log(
        '=====check editable value(this.updatedRecievedAmount)======',
        this.updatedRecievedAmount
      );
    }
    // this.isSwitch = !this.isSwitch;
  }

  setAddMode(showAddBox): void {
    this.isAddMode = showAddBox;
    if (showAddBox === true) {
      this.recievedAmount = null;
      this.date = new FormControl(new Date());
      this.currentEditingPayment = null;
      this.selectedPaymentMode = 'CHEQUE';
      this.transactionRefNo = null;
    }
  }

  onGetProjectDetails(): void {
    this.isLoading = true;
    this.subscription = this.projectService
      .getProjectDetails(this.newID)
      .subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            const projectDetails: ProjectDetailsData = new ProjectDetailsData();
            if (response.code) {
              projectDetails.projectValue = response.data.projectModularValue;
              projectDetails.signUpAmount = response.data.signUpAmount;
              projectDetails.totalProjectValue =
                response.data.totalProjectValue;
              projectDetails.latestBOQ = response.data.latestBOQ;
              projectDetails.deepCleaningEnabled =
                response.data.isDeepCleaningEnabled;
              projectDetails.deepCleaning = response.data.deepCleaningValue;
              projectDetails.addonValue = response.data.addOnValue;
              projectDetails.achievedRevenue =
                response.data.achievedRevenueValue;
              projectDetails.pendingAmount = response.data.pendingAmountValue;
              projectDetails.addonCosting = response.data.addOnCosting;
            }
            this.newData = projectDetails;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onupdateTeamMember(): void {
    const requestProjectDetailsData: ProjectDetailsData = new ProjectDetailsData();
    requestProjectDetailsData.id = this.newID;
    requestProjectDetailsData.projectModularValue = this.projectModularValue;
    requestProjectDetailsData.addOnValue = this.addOnValue;
    this.subscription = this.projectService
      .updateProjectCommercial(requestProjectDetailsData)
      .subscribe(
        (response) => {
          if (response.code === 200) {
            this.isShown = false;
            // this.currentEditingPayment = null;
            this.projectModularValue = null;
            this.addOnValue = null;
            this.snackBar.open('Updated Successfully', 'Close', {
              duration: 1500,
            });
            this.onGetProjectDetails();
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onGetPaymentMilestoneList(): void {
    this.isLoading = true;
    this.subscription = this.paymentService
      .getPaymentMilestoneList(this.newID)
      .subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            const projectPaymentList: PaymentListDataModel[] = [];
            if (response.data && response.data.paymentMilestones) {
              response.data.paymentMilestones.forEach((element) => {
                const paymentList: PaymentListDataModel = new PaymentListDataModel();
                paymentList._id = element._id;
                paymentList.milestoneName = element.milestoneName;
                paymentList.dueDate = element.dueDate;
                paymentList.paymentValue = element.paymentValue;
                paymentList.totalPayableAmount = element.totalPayableAmount;
                paymentList.amountPaid = element.amountPaid;
                paymentList.amountDue = element.amountDue;
                paymentList.status = element.status;
                projectPaymentList.push(paymentList);
              });
            }
            this.projectPaymentListArr = projectPaymentList;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onGetPaymentMilestoneDetails(): void {
    this.isLoading = true;
    this.subscription = this.paymentService
      .getPaymentMilestoneDetails(this.paymentID)
      .subscribe(
        (response) => {
          this.isLoading = false;
          const paymentConfirmationArr: PaymentConfirmationDataModel[] = [];
          if (response.code === 200) {
            const paymentDetailsData: PaymentDetailsDataModel = new PaymentDetailsDataModel();
            if (response.data && response.data.paymentMilestone) {
              paymentDetailsData.milestoneAmount =
                response.data.paymentMilestone.milestoneAmount;
              paymentDetailsData.advanceAmount =
                response.data.paymentMilestone.advanceAmount;
              paymentDetailsData.addonAmount =
                response.data.paymentMilestone.addonAmount;
              paymentDetailsData.totalPayableAmount =
                response.data.paymentMilestone.totalPayableAmount;
              paymentDetailsData.amountPaid =
                response.data.paymentMilestone.amountPaid;
              paymentDetailsData.amountDue =
                response.data.paymentMilestone.amountDue;
              paymentDetailsData.isFreezed =
                response.data.paymentMilestone.isFreezed;
              console.log('paymentDetailsData', paymentDetailsData);
              paymentDetailsData.invoiceBaseAmount =
                response.data.paymentMilestone.invoiceBaseAmount;
              paymentDetailsData.invoiceNumber =
                response.data.paymentMilestone.invoiceNumber;
              paymentDetailsData.invoiceTotalAmount =
                response.data.paymentMilestone.invoiceTotalAmount;
              paymentDetailsData.invoiceTotalTaxAmount =
                response.data.paymentMilestone.invoiceTotalTaxAmount;
              paymentDetailsData.invoiceDate =
                response.data.paymentMilestone.invoiceDate;
              paymentDetailsData.invoiceURL =
                response.data.paymentMilestone.invoiceURL;
              paymentDetailsData.paymentLink =
                response.data.paymentMilestone.paymentLink;

              // const generateInvoice: GeneratePaymentInvoiceListRequestModel = new GeneratePaymentInvoiceListRequestModel();
              // generateInvoice.invoiceBaseAmount = response.data.paymentMilestone.invoiceBaseAmount;
              // generateInvoice.invoiceNumber = response.data.paymentMilestone.invoiceNumber;
              // generateInvoice.invoiceTotalAmount = response.data.paymentMilestone.invoiceTotalAmount;
              // generateInvoice.invoiceTotalTaxAmount = response.data.paymentMilestone.invoiceTotalTaxAmount;
              // generateInvoice.invoiceDate = response.data.paymentMilestone.invoiceDate;
              // generateInvoice.invoiceURL = response.data.paymentMilestone.invoiceURL;
              // generateInvoice.paymentLink = response.data.paymentMilestone.paymentLink;

              if (response.data.paymentMilestone.paymentConfirmation) {
                response.data.paymentMilestone.paymentConfirmation.forEach(
                  (element) => {
                    const paymentConfirmation: PaymentConfirmationDataModel = new PaymentConfirmationDataModel();
                    paymentConfirmation._id = element._id;
                    paymentConfirmation.paymentName = element.paymentName;
                    paymentConfirmation.receivedAmount = element.receivedAmount;
                    paymentConfirmation.receivedDate = element.receivedDate;
                    paymentConfirmation.modeOfPayment = this.getPaymentModeEnum(
                      element.modeOfPayment
                    );
                    paymentConfirmation.transactionRefNo =
                      element.transactionRefNo;
                    paymentConfirmation.supportingDocumentURL =
                      element.supportingDocumentURL;
                    paymentConfirmation.awsSupportingDocumentURLKey =
                      element.awsSupportingDocumentURLKey;
                    paymentConfirmationArr.push(paymentConfirmation);
                  }
                );
              }
              this.mainPaymentConfirmationArr = paymentConfirmationArr;
            }
            this.paymentDetailsItem = paymentDetailsData;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onAddPaymentConfirmation(): void {
    const requestpaymentConfirmationData: AddPaymentConfirmationDataModel = new AddPaymentConfirmationDataModel();
    requestpaymentConfirmationData.receivedDate = this.updatedDate;
    requestpaymentConfirmationData.receivedAmount = this.recievedAmount;
    requestpaymentConfirmationData.transactionRefNo = this.transactionRefNo;
    requestpaymentConfirmationData.modeOfPayment = this.getPaymentMode(
      this.selectedPaymentMode
    );
    requestpaymentConfirmationData.projectID = this.newID;
    requestpaymentConfirmationData.paymentMilestoneObjectID = this.paymentID;
    if (this.fileName && this.contentType && this.base64textString) {
      // console.log('======after Check filename, content and base4', this.fileName, this.contentType, this.base64textString);
      const requestAttachmentFile: AddPaymentAttachmentFileRequestModel = new AddPaymentAttachmentFileRequestModel();
      requestAttachmentFile.fileName = this.fileName;
      requestAttachmentFile.contentType = this.contentType;
      requestAttachmentFile.fileBase64 = this.base64textString;
      requestpaymentConfirmationData.attachment = requestAttachmentFile;
      // console.log('======attachment model=====', requestMomData.attachment, requestAttachmentFile);
    }
    this.subscription = this.paymentService
      .addPaymentConfirmation(requestpaymentConfirmationData)
      .subscribe(
        (response) => {
          if (response.code === 200) {
            this.isAddMode = false;
            this.recievedAmount = null;
            this.transactionRefNo = null;
            this.fileName = null;
            this.contentType = null;
            this.base64textString = null;
            this.snackBar.open('Added Successfully', 'Close', {
              duration: 1500,
            });
            this.onGetPaymentMilestoneDetails();
            this.supportingDocumentURL = response.data.supportingDocumentURL;
            this.awsSupportingDocumentURLKey =
              response.data.awsSupportingDocumentURLKey;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onUpdatePaymentConfirmation(): void {
    const requestpaymentConfirmationData: UpdatePaymentConfirmationDataModel = new UpdatePaymentConfirmationDataModel();
    requestpaymentConfirmationData.receivedDate = this.updatedDate;
    requestpaymentConfirmationData.receivedAmount = this.updatedRecievedAmount;
    requestpaymentConfirmationData.transactionRefNo = this.updatedTransactionRefNo;
    requestpaymentConfirmationData.modeOfPayment = this.getPaymentMode(
      this.selectedPaymentMode
    );
    requestpaymentConfirmationData.projectID = this.newID;
    requestpaymentConfirmationData.paymentMilestoneObjectID = this.paymentID;
    requestpaymentConfirmationData.paymentConfirmationObjectID = this.currentEditingPayment;
    if (this.fileName && this.contentType && this.base64textString) {
      // console.log('======after Check filename, content and base4', this.fileName, this.contentType, this.base64textString);
      const requestAttachmentFile: UpdatePaymentAttachmentFileRequestModel = new UpdatePaymentAttachmentFileRequestModel();
      requestAttachmentFile.fileName = this.fileName;
      requestAttachmentFile.contentType = this.contentType;
      requestAttachmentFile.fileBase64 = this.base64textString;
      requestpaymentConfirmationData.attachment = requestAttachmentFile;
      // console.log('======attachment model=====', requestMomData.attachment, requestAttachmentFile);
    }
    this.subscription = this.paymentService
      .updatePaymentConfirmation(requestpaymentConfirmationData)
      .subscribe(
        (response) => {
          if (response.code === 200) {
            this.currentEditingPayment = null;
            this.updatedRecievedAmount = null;
            this.updatedTransactionRefNo = null;
            this.fileName = null;
            this.contentType = null;
            this.base64textString = null;
            this.snackBar.open('Updated Successfully', 'Close', {
              duration: 1500,
            });
            this.onGetPaymentMilestoneDetails();
            this.supportingDocumentURL = null;
            this.awsSupportingDocumentURLKey = null;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onGeneratePaymentInvoice(): void {
    const requestGenerateInvoice: GeneratePaymentInvoiceRequestModel = new GeneratePaymentInvoiceRequestModel();
    requestGenerateInvoice.milestoneID = this.paymentID;
    requestGenerateInvoice.projectID = this.newID;
    requestGenerateInvoice.isRegenerate = this.paymentDetailsItem.invoiceNumber
      ? true
      : false;
    this.subscription = this.paymentService
      .generatePaymentInvoice(requestGenerateInvoice)
      .subscribe(
        (response) => {
          if (response.code === 200) {
            this.snackBar.open('Invoice Generated Successfully', 'Close', {
              duration: 1500,
            });
            this.onGetPaymentMilestoneDetails();
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onFileSelected(event): void {
    console.log(event.target.files[0]);
    const files = event.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this.handleFile.bind(this);
      reader.readAsBinaryString(file);
      this.fileName = file.name;
      this.contentType = file.type;
      this.awsSupportingDocumentURLKey = file.name;
    }
  }

  handleFile(event) {
    var binaryString = event.target.result;
    this.base64textString = btoa(binaryString);
    // console.log(this.base64textString);
  }

  getPaymentMode(paymentMode: string): number {
    switch (paymentMode) {
      case 'CHEQUE':
        return 0;
      case 'CASH':
        return 1;
      case 'IMPS':
        return 2;
      case 'NEFT':
        return 3;
      case 'ONLINE':
        return 4;
    }
  }

  getPaymentModeEnum(paymentModeEnumCode: number): string {
    switch (paymentModeEnumCode) {
      case 0:
        return 'CHEQUE';
      case 1:
        return 'CASH';
      case 2:
        return 'IMPS';
      case 3:
        return 'NEFT';
      case 4:
        return 'ONLINE';
      default:
        return '-';
    }
  }

  addEvent(event: MatDatepickerInputEvent<Date>): void {
    const selectedDate = moment(event.value).format('DD/MM/YYYY');
    this.updatedDate = selectedDate;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

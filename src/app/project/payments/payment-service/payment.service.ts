import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { AddPaymentConfirmationDataModel } from '../payment-data/addpaymentconfirmationdata';
import { GeneratePaymentInvoiceRequestModel } from '../payment-data/generatepaymentinvoice';
import { UpdatePaymentConfirmationDataModel } from '../payment-data/updatepaymentconfirmationdata';

@Injectable({providedIn: 'root'})
export class PaymentService {

    constructor(private http: HttpClient, private baseUrl: BaseUrl, private localStorage: LocalStorage) {}

    public getToken = this.localStorage.getAccessToken();
    public header = new HttpHeaders({ 'Authorization': this.getToken });

    getPaymentMilestoneList(id: string): Observable<any> {
        const param = new HttpParams().set('projectID', id);
        return this.http.get(this.baseUrl.getFinalUrl('getPaymentMilestoneList'), {headers: this.header, params: param});
    }

    getPaymentMilestoneDetails(id: string): Observable<any> {
        const param = new HttpParams().set('id', id);
        return this.http.get(this.baseUrl.getFinalUrl('getPaymentMilestoneDetails'), {headers: this.header, params: param});
    }

    addPaymentConfirmation(addPaymentConfirmationData: AddPaymentConfirmationDataModel): Observable<any> {
        return this.http.post(this.baseUrl.getFinalUrl('addPaymentConfirmation'), addPaymentConfirmationData);
    }

    updatePaymentConfirmation(updatePaymentConfirmationData: UpdatePaymentConfirmationDataModel): Observable<any> {
        return this.http.put(this.baseUrl.getFinalUrl('updatePaymentConfirmation'), updatePaymentConfirmationData,
        {
            headers: this.header
        }
        );
    }
    
    generatePaymentInvoice(generateInvoice: GeneratePaymentInvoiceRequestModel): Observable<any> {
        return this.http.post(this.baseUrl.getFinalUrl('generatePaymentInvoice'), generateInvoice, {headers: this.header});
    }
}

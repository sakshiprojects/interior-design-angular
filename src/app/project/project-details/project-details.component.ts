import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { ProjectDetailsData } from '../project-data/project-details-data';
import { ProjectService } from '../project-service/project.service';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, distinctUntilChanged, find, map } from 'rxjs/operators';
import { FindUserDataRequestModel } from '../project-data/finduserdata';
import { UpdateTeamMemberRequestModel } from '../project-data/updateteammembersdata';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'],
})
export class ProjectDetailsComponent implements OnInit, OnDestroy {
  public model: any;
  public projectItem;
  public newItem;
  public newID;
  public projectModularValue: string;
  public addOnValue: string;
  public designer: string;
  public designerID: string = null;
  public CHM: string;
  public CHMID: string = null;
  public salesExecutive: string;
  public salesExecutiveID: string = null;
  public surveyExecutive: string;
  public surveyExecutiveID: string = null;
  public threeDDesignerShell: string;
  public threeDDesignerShellID: string = null;
  public threeDDesignerRenders: string;
  public threeDDesignerRendersID: string = null;
  public key: string = null;
  public usersData = [];
  // suggestions: string[] = ['a', 'b', 'c', 'd'];
  subscription: Subscription;
  public isShown = false;
  public showData = false;
  public isLoading = false;
  // formatter = (result: string) => result.toUpperCase();

  //

  constructor(
    private projectService: ProjectService,
    private router: Router,
    private localStorage: LocalStorage,
    private snackBar: MatSnackBar,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.projectItem = JSON.parse(localStorage.getItem('projectData'));
    this.newID = this.projectItem._id;
    console.log(
      '======projectData Log======',
      JSON.parse(localStorage.getItem('projectData'))
    );
    console.log('======projectData ID======', this.newID);
    this.ongetProjectDetails();
  }

  onChange(modulerValue, addOnValue): void {
    this.isShown = !this.isShown;
    this.projectModularValue = modulerValue;
    this.addOnValue = addOnValue;
    console.log('====proj details===', modulerValue, this.projectModularValue);
  }

  onSwitch(
    designer,
    CHM,
    salesExecutive,
    surveyExecutive,
    threeDDesignerShell,
    threeDDesignerRenders
  ): void {
    this.showData = !this.showData;
    this.designer = designer;
    this.CHM = CHM;
    this.salesExecutive = salesExecutive;
    this.surveyExecutive = surveyExecutive;
    this.threeDDesignerShell = threeDDesignerShell;
    this.threeDDesignerRenders = threeDDesignerRenders;
  }

  ongetProjectDetails(): void {
    this.isLoading = true;
    this.subscription = this.projectService
      .getProjectDetails(this.newID)
      .subscribe(
        (response) => {
          this.isLoading = false;
          if (response.code === 200) {
            // this.newItem = response.data;
            const projectDetails: ProjectDetailsData = new ProjectDetailsData();
            if (response.data) {
              // projectDetails._id = response.data._id;
              // this.designerID = response.data._id;
              projectDetails.name = response.data.projectName;
              projectDetails.customerID = response.data.customer.sfID;
              projectDetails.signUpDate = response.data.signupDate;
              projectDetails.plannedEndDate = response.data.plannedEndDate;
              projectDetails.meetingVenue = response.data.meetingVenue;
              if (response.data.designTeam && response.data.designTeam.name) {
                projectDetails.teamName = response.data.designTeam.name;
              }
              if (
                response.data.leadOwner &&
                response.data.leadOwner.firstName
              ) {
                projectDetails.leadOwner =
                  response.data.leadOwner.firstName +
                  (response.data.leadOwner.middleName
                    ? `${response.data.leadOwner.middleName}`
                    : '') +
                  (response.data.leadOwner.lastName
                    ? `${response.data.leadOwner.lastName}`
                    : '');
              }
              projectDetails.region = response.data.region;

              projectDetails.projectValue = response.data.projectModularValue;
              projectDetails.signUpAmount = response.data.signUpAmount;
              projectDetails.totalProjectValue =
                response.data.totalProjectValue;
              projectDetails.latestBOQ = response.data.latestBOQ;
              projectDetails.deepCleaningEnabled =
                response.data.isDeepCleaningEnabled;
              projectDetails.deepCleaning = response.data.deepCleaningValue;
              projectDetails.addonValue = response.data.addOnValue;
              projectDetails.achievedRevenue =
                response.data.achievedRevenueValue;
              projectDetails.pendingAmount = response.data.pendingAmountValue;
              projectDetails.addonCosting = response.data.addOnCosting;
              projectDetails.offersAndDiscounts =
                response.data.offerAndDiscount;

              if (response.data.customer) {
                if (response.data.customer.name) {
                  projectDetails.customerName = response.data.customer.name;
                }
                if (response.data.customer.phoneNumber) {
                  projectDetails.contactNo = response.data.customer.phoneNumber;
                }
                if (
                  response.data.customer.shippingAddressCountry ||
                  response.data.customer.shippingAddressStreet ||
                  response.data.customer.shippingAddressCity ||
                  response.data.customer.shippingAddressPostalCode ||
                  response.data.customer.shippingAddressState
                ) {
                  projectDetails.shippingAddress =
                    (response.data.customer.shippingAddressCountry
                      ? ` ${response.data.customer.shippingAddressCountry} `
                      : '') +
                    (response.data.customer.shippingAddressStreet
                      ? ` ${response.data.customer.shippingAddressStreet} `
                      : '') +
                    (response.data.customer.shippingAddressCity
                      ? ` ${response.data.customer.shippingAddressCity} `
                      : '') +
                    (response.data.customer.shippingAddressPostalCode
                      ? ` ${response.data.customer.shippingAddressPostalCode} `
                      : '') +
                    (response.data.customer.shippingAddressState
                      ? ` ${response.data.customer.shippingAddressState} `
                      : '');
                }
                if (response.data.customer.emailID) {
                  projectDetails.email = response.data.customer.emailID;
                }
                if (
                  response.data.customer.shippingAddressCountry ||
                  response.data.customer.shippingAddressStreet ||
                  response.data.customer.shippingAddressCity ||
                  response.data.customer.shippingAddressPostalCode ||
                  response.data.customer.shippingAddressState
                ) {
                  projectDetails.billingAddress =
                    (response.data.customer.billingAddressCountry
                      ? ` ${response.data.customer.billingAddressCountry} `
                      : '') +
                    (response.data.customer.billingAddressStreet
                      ? ` ${response.data.customer.billingAddressStreet} `
                      : '') +
                    (response.data.customer.billingAddressCity
                      ? ` ${response.data.customer.billingAddressCity} `
                      : '') +
                    (response.data.customer.billingAddressPostalCode
                      ? ` ${response.data.customer.billingAddressPostalCode} `
                      : '') +
                    (response.data.customer.billingAddressState
                      ? ` ${response.data.customer.billingAddressState} `
                      : '');
                }
              }

              if (response.data.designer && response.data.designer.firstName) {
                projectDetails.designer =
                  response.data.designer.firstName +
                  (response.data.designer.middleName
                    ? ` ${response.data.designer.middleName} `
                    : '') +
                  (response.data.designer.lastName
                    ? ` ${response.data.designer.lastName} `
                    : '');
              }
              if (
                response.data.salesExecutive &&
                response.data.salesExecutive.firstName
              ) {
                projectDetails.salesExecutive =
                  response.data.salesExecutive.firstName +
                  (response.data.salesExecutive.middleName
                    ? ` ${response.data.salesExecutive.middleName} `
                    : '') +
                  (response.data.salesExecutive.lastName
                    ? ` ${response.data.salesExecutive.lastName} `
                    : '');
              }
              if (
                response.data.threeDDesignShell &&
                response.data.threeDDesignShell.firstName
              ) {
                projectDetails.threeDDesignerShell =
                  response.data.threeDDesignShell.firstName +
                  (response.data.threeDDesignShell.middleName
                    ? ` ${response.data.threeDDesignShell.middleName} `
                    : '') +
                  (response.data.threeDDesignShell.lastName
                    ? ` ${response.data.threeDDesignShell.lastName} `
                    : '');
              }
              if (
                response.data.chmExecutive &&
                response.data.chmExecutive.firstName
              ) {
                projectDetails.CHM =
                  response.data.chmExecutive.firstName +
                  (response.data.chmExecutive.middleName
                    ? ` ${response.data.chmExecutive.middleName} `
                    : '') +
                  (response.data.chmExecutive.lastName
                    ? ` ${response.data.chmExecutive.lastName} `
                    : '');
              }
              if (
                response.data.surveyExecutive &&
                response.data.surveyExecutive.firstName
              ) {
                projectDetails.surveyExecutive =
                  response.data.surveyExecutive.firstName +
                  (response.data.surveyExecutive.middleName
                    ? ` ${response.data.surveyExecutive.middleName} `
                    : '') +
                  (response.data.surveyExecutive.lastName
                    ? ` ${response.data.surveyExecutive.lastName} `
                    : '');
              }
              if (
                response.data.threeDDesignRender &&
                response.data.threeDDesignRender.firstName
              ) {
                projectDetails.threeDDesignerRenders =
                  response.data.threeDDesignRender.firstName +
                  (response.data.threeDDesignRender.middleName
                    ? ` ${response.data.threeDDesignRender.middleName} `
                    : '') +
                  (response.data.threeDDesignRender.lastName
                    ? ` ${response.data.threeDDesignRender.lastName} `
                    : '');
              }
            }
            this.newItem = projectDetails;
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onUpdateProjectCommercial(): void {
    const requestProjectDetailsData: ProjectDetailsData = new ProjectDetailsData();
    requestProjectDetailsData.id = this.newID;
    requestProjectDetailsData.projectModularValue = this.projectModularValue;
    requestProjectDetailsData.addOnValue = this.addOnValue;
    // requestProjectDetailsData.designer = this.designer;
    this.subscription = this.projectService
      .updateProjectCommercial(requestProjectDetailsData)
      .subscribe(
        (response) => {
          if (response.code === 200) {
            this.isShown = false;
            // this.currentEditingPayment = null;
            this.projectModularValue = null;
            this.addOnValue = null;
            this.snackBar.open('Updated Successfully', 'Close', {
              duration: 1500,
            });
            this.ongetProjectDetails();
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  onUpdateTeamDetails(): void {
    const requestProjectDetailsData: UpdateTeamMemberRequestModel = new UpdateTeamMemberRequestModel();
    // const findUser: FindUserDataRequestModel = new FindUserDataRequestModel();
    requestProjectDetailsData.id = this.newID;
    if (this.designerID) {
      requestProjectDetailsData.designer = this.designerID;
    }
    if (this.salesExecutiveID) {
      requestProjectDetailsData.salesExecutive = this.salesExecutiveID;
    }
    if (this.threeDDesignerShellID) {
      requestProjectDetailsData.threeDDesignShell = this.threeDDesignerShellID;
    }
    if (this.CHMID) {
      requestProjectDetailsData.chmExecutive = this.CHMID;
    }
    if (this.surveyExecutiveID) {
      requestProjectDetailsData.surveyExecutive = this.surveyExecutiveID;
    }
    if (this.threeDDesignerRendersID) {
      requestProjectDetailsData.threeDDesignRender = this.threeDDesignerRendersID;
    }
    this.subscription = this.projectService
      .updateTeamMember(requestProjectDetailsData)
      .subscribe(
        (response) => {
          if (response.code === 200) {
            this.showData = false;
            this.designer = null;
            this.snackBar.open('Updated Successfully', 'Close', {
              duration: 1500,
            });
            this.ongetProjectDetails();
          } else {
            this.snackBar.open(response.message, 'Close', {
              duration: 1500,
            });
          }
        },
        (errorMessage) => {
          console.log('CORS Error', errorMessage);
          if (errorMessage.statusText == 'Unknown Error') {
            this.toastr.error(
              'Some unknown error occured. Please contact administrator'
            );
            this.isLoading = false;
          }
        }
      );
  }

  findUserApi(event): void {
    const userArr = [];
    this.subscription = this.projectService.findUserApiUseCase(event).subscribe(
      (response) => {
        if (response.code === 200) {
          // this.key = null;
          if (response.data.users) {
            response.data.users.forEach((element) => {
              const findUser: FindUserDataRequestModel = new FindUserDataRequestModel();
              findUser.id = element.id;
              findUser.name = element.name;
              userArr.push(findUser);
            });
          }

          // console.log('====== check event-------',);
        } else {
          this.snackBar.open(response.message, 'Close', {
            duration: 1500,
          });
        }
        this.usersData = userArr;
      },
      (errorMessage) => {
        console.log('CORS Error', errorMessage);
        if (errorMessage.statusText == 'Unknown Error') {
          this.toastr.error(
            'Some unknown error occured. Please contact administrator'
          );
        }
      }
    );
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(800),
      distinctUntilChanged(),
      map((term) => {
        this.findUserApi(term);
        // return this.usersData.map(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)
        return this.usersData.map((x) => x.name);
      })
    );

  selectedItem(event, type) {
    let aa = this.usersData.find((x) => x.name == event.item);

    if (type == 0) {
      this.designerID = aa.id;
    }

    if (type == 1) {
      this.salesExecutiveID = aa.id;
    }

    if (type == 2) {
      this.threeDDesignerShellID = aa.id;
    }

    if (type == 3) {
      this.CHMID = aa.id;
    }

    if (type == 4) {
      this.surveyExecutiveID = aa.id;
    }

    if (type == 5) {
      this.threeDDesignerRendersID = aa.id;
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

// search = (text$: Observable<string>) =>
//   text$.pipe(
//     debounceTime(200),
//     distinctUntilChanged(),
//     map(term => term.length < 2 ? []
//       : states.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
//   )

// ongetProjectList(): void {
//   this.isLoading = true;
//   this.projectService.getProjectList().subscribe(response => {
//     this.isLoading = false;
//     if (response.code === 200) {
//       this.projectItemArr = response.data.projects.data;
//     } else if (response.code >= 400) {
//       this.localStorage.clearAccessToken();
//       this.router.navigate(['/login']);
//       this.snackBar.open('Session Expired', 'close', {
//         duration: 1200,
//       });
//     }
//   });
// }

// switch (response.data.customer) {
//   case (response.data.customer.name):
//     projectDetails.customerName = response.data.customer.name;
//     break;
//   case (response.data.customer.phoneNumber):
//     projectDetails.contactNo = response.data.customer.phoneNumber;
//     break;
//   case (
//           response.data.customer.shippingAddressCountry ||
//           response.data.customer.shippingAddressStreet ||
//           response.data.customer.shippingAddressCity ||
//           response.data.customer.shippingAddressPostalCode ||
//           response.data.customer.shippingAddressState
//           ):
//     projectDetails.shippingAddress =
//               (response.data.customer.shippingAddressCountry ? ` ${response.data.customer.shippingAddressCountry} ` : '') +
//               (response.data.customer.shippingAddressStreet ? ` ${response.data.customer.shippingAddressStreet} ` : '') +
//               (response.data.customer.shippingAddressCity ? ` ${response.data.customer.shippingAddressCity} ` : '') +
//               (response.data.customer.shippingAddressPostalCode ? ` ${response.data.customer.shippingAddressPostalCode} ` : '') +
//               (response.data.customer.shippingAddressState ? ` ${response.data.customer.shippingAddressState} ` : '');
//     break;
//    case (response.data.customer.emailID):
//     projectDetails.email = response.data.customer.emailID;
//     break;
//    case (
//           response.data.customer.shippingAddressCountry ||
//           response.data.customer.shippingAddressStreet ||
//           response.data.customer.shippingAddressCity ||
//           response.data.customer.shippingAddressPostalCode ||
//           response.data.customer.shippingAddressState
//       ):
//     projectDetails.billingAddress =
//               (response.data.customer.billingAddressCountry ? ` ${response.data.customer.billingAddressCountry} ` : '') +
//               (response.data.customer.billingAddressStreet ? ` ${response.data.customer.billingAddressStreet} ` : '') +
//               (response.data.customer.billingAddressCity ? ` ${response.data.customer.billingAddressCity} ` : '') +
//               (response.data.customer.billingAddressPostalCode ? ` ${response.data.customer.billingAddressPostalCode} ` : '') +
//               (response.data.customer.billingAddressState ? ` ${response.data.customer.billingAddressState} ` : '');
//     break;
// }

// stateListData = [{ "name": "Alabama", "abbreviation": "AL" }, { "name": "Alaska", "abbreviation": "AK" }, { "name": "AmericanSamoa", "abbreviation": "AS" }, { "name": "Arizona", "abbreviation": "AZ" }, { "name": "Arkansas", "abbreviation": "AR" }, { "name": "California", "abbreviation": "CA" }, { "name": "Colorado", "abbreviation": "CO" }, { "name": "Connecticut", "abbreviation": "CT" }, { "name": "Delaware", "abbreviation": "DE" }, { "name": "DistrictOfColumbia", "abbreviation": "DC" }, { "name": "FederatedStatesOfMicronesia", "abbreviation": "FM" }, { "name": "Florida", "abbreviation": "FL" }, { "name": "Georgia", "abbreviation": "GA" }, { "name": "Guam", "abbreviation": "GU" }, { "name": "Hawaii", "abbreviation": "HI" }, { "name": "Idaho", "abbreviation": "ID" }, { "name": "Illinois", "abbreviation": "IL" }, { "name": "Indiana", "abbreviation": "IN" }, { "name": "Iowa", "abbreviation": "IA" }, { "name": "Kansas", "abbreviation": "KS" }, { "name": "Kentucky", "abbreviation": "KY" }, { "name": "Louisiana", "abbreviation": "LA" }, { "name": "Maine", "abbreviation": "ME" }, { "name": "MarshallIslands", "abbreviation": "MH" }, { "name": "Maryland", "abbreviation": "MD" }, { "name": "Massachusetts", "abbreviation": "MA" }, { "name": "Michigan", "abbreviation": "MI" }, { "name": "Minnesota", "abbreviation": "MN" }, { "name": "Mississippi", "abbreviation": "MS" }, { "name": "Missouri", "abbreviation": "MO" }, { "name": "Montana", "abbreviation": "MT" }, { "name": "Nebraska", "abbreviation": "NE" }, { "name": "Nevada", "abbreviation": "NV" }, { "name": "NewHampshire", "abbreviation": "NH" }, { "name": "NewJersey", "abbreviation": "NJ" }, { "name": "NewMexico", "abbreviation": "NM" }, { "name": "NewYork", "abbreviation": "NY" }, { "name": "NorthCarolina", "abbreviation": "NC" }, { "name": "NorthDakota", "abbreviation": "ND" }, { "name": "NorthernMarianaIslands", "abbreviation": "MP" }, { "name": "Ohio", "abbreviation": "OH" }, { "name": "Oklahoma", "abbreviation": "OK" }, { "name": "Oregon", "abbreviation": "OR" }, { "name": "Palau", "abbreviation": "PW" }, { "name": "Pennsylvania", "abbreviation": "PA" }, { "name": "PuertoRico", "abbreviation": "PR" }, { "name": "RhodeIsland", "abbreviation": "RI" }, { "name": "SouthCarolina", "abbreviation": "SC" }, { "name": "SouthDakota", "abbreviation": "SD" }, { "name": "Tennessee", "abbreviation": "TN" }, { "name": "Texas", "abbreviation": "TX" }, { "name": "Utah", "abbreviation": "UT" }, { "name": "Vermont", "abbreviation": "VT" }, { "name": "VirginIslands", "abbreviation": "VI" }, { "name": "Virginia", "abbreviation": "VA" }, { "name": "Washington", "abbreviation": "WA" }, { "name": "WestVirginia", "abbreviation": "WV" }, { "name": "Wisconsin", "abbreviation": "WI" }, { "name": "Wyoming", "abbreviation": "WY" }];
// typeAheadSetup = {
//   customTemplate: '<div> {{item.name}}</div>',
//   //    timeDelay: number;
//   // type: 'static', //static || dynamic.  default value is dynamic
//   placeHolder: 'State name',
//   textPrperty: 'name',
//   valueProperty: 'abbreviation',
//   searchProperty: 'name',
//   onSelect: (selectedItem: any) => { console.log(selectedItem) },
//   asynchDataCall: (value: string, cb: any) => {
//     let result = this.stateListData.filter((item: any) => {
//       return item.name.indexOf(value) !== -1;
//     });
//   //you can place your webservice call here
//     setTimeout(() => {
//       cb(result);
//     }, 200);
//   },
//   //staticDataFilterkey: any;
//   //staticData: stateListData
// }

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { MarkProjectMilestoneCompleteRequestModel } from './project-milestone-data/projectmilestonecompletedata';
import { ProjectMilestoneData } from './project-milestone-data/projectmilestonedata';
import { UpdateProjectMilestoneRequestModel } from './project-milestone-data/updateprojectmilestonedata';
import { ProjectMilestoneService } from './project-milestone-service/projectmilestone.service';
import { ToastrService } from 'ngx-toastr';
// import { ProjectMilestoneService } from './project-milestone-service/projectmilestone.service';
// import { ProjectMilestoneData } from './project-milestone-data/projectmilestonedata';

@Component({
  selector: 'app-project-milestones',
  templateUrl: './project-milestones.component.html',
  styleUrls: ['./project-milestones.component.css']
})
export class ProjectMilestonesComponent implements OnInit, OnDestroy {
  projectMilestoneListArr: ProjectMilestoneData[] = [];
  projectID;
  projectItem;
  subscription: Subscription;
  // isShown = false;
  currentEditingMilestone = null;
  updatedDate = null;
  // public date;
  isLoading = false;
  apiResponse: ProjectMilestoneData[] = [];
  phase = 0;
  date = new FormControl(new Date());

  constructor(
    private router: Router,
    private projectMilestoneService: ProjectMilestoneService,
    private snackBar: MatSnackBar,
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    // this.router.navigate(['project/projectLinks/projectMilestones'], { skipLocationChange: true });
    // this.router.navigate(['project/projectLinks/projectMilestones/preDesignStage'], { skipLocationChange: true });
    this.projectItem = JSON.parse(localStorage.getItem('projectData'));
    this.projectID = this.projectItem._id;
    this.getProjectMilestoneList();
  }

  onChange(id, plannedEndDate): void {
    // this.date = new FormControl(moment(plannedEndDate, 'DD/MM/YYYY').toDate());
    this.projectMilestoneListArr.forEach(element => {
      if (element.id  === id) {
    this.date = new FormControl(moment(element.plannedEndDate, 'DD/MM/YYYY').toDate());
      }
    });
    this.currentEditingMilestone = id;
    this.updatedDate = null;
    // this.isShown = !this.isShown;
  }

  getProjectMilestoneList(): void {
    this.currentEditingMilestone = null;
    this.isLoading = true;
    this.subscription = this.projectMilestoneService.getProjectMilestone(this.projectID).subscribe(response => {
      this.isLoading = false;
      if (response.code === 200) {
        const projectMilestoneArr: ProjectMilestoneData[] = [];
        if (response.data && response.data.projectMilestones) {
          // this.apiResponse = response.data.projectMilestones;
          // ===== segregate name function
          // this.apiResponse.forEach(element => {
            response.data.projectMilestones.forEach(element => {
            const projectMilestone: ProjectMilestoneData = new ProjectMilestoneData();
            // if (element.position >= 0) {
            projectMilestone.id = element._id;
            projectMilestone.position = element.position;
            projectMilestone.milestoneName = element.milestoneName;
            if (element.assignedTo && element.assignedTo.firstName) {
              projectMilestone.assignedTo =
              element.assignedTo.firstName +
              (element.assignedTo.middleName ? ` ${element.assignedTo.middleName} ` : ' ') +
              (element.assignedTo.lastName ? ` ${element.assignedTo.lastName} ` : ' ');
            }
            if (element.team && element.team.name) {
              projectMilestone.teamName = element.team.name;
            }
            projectMilestone.startDate = element.startDate;
            projectMilestone.plannedEndDate = element.plannedEndDate;
            // if (element.plannedEndDate) {
            //   this.date = new FormControl(moment(element.plannedEndDate, 'DD/MM/YYYY').toDate());
            // }
            // this.date = new FormControl(moment(projectMilestone.plannedEndDate, 'DD/MM/YYYY').toDate());
            // this.date = new FormControl(moment([projectMilestone.plannedEndDate ]));
            projectMilestone.markAsComplete = element.isCompleted;
            projectMilestone.completionDate = element.completionDate;
            // if (element.position >= 0 && element.position < 3) {
              projectMilestoneArr.push(projectMilestone);
            // }
            // }
          });
        }
        // this.projectMilestoneListArr = projectMilestoneArr;
        this.apiResponse = projectMilestoneArr;
        if (this.phase == 0){
        this.segregateMilestoneForPhases(0,2,0);
      }
        if (this.phase == 1){
        this.segregateMilestoneForPhases(3,14,1);
      }
        if (this.phase == 2){
        this.segregateMilestoneForPhases(15,50,2);
      }
      } else {
        this.snackBar.open(response.message, 'close', {
          duration: 1500,
        });
      }
    },
    errorMessage => {
      console.log("CORS Error",errorMessage);
      if(errorMessage.statusText == "Unknown Error"){
      this.toastr.error('Some unknown error occured. Please contact administrator');
      this.isLoading = false
    }
    }
    );
  }

  segregateMilestoneForPhases(start, end, phase): void {
    this.phase = phase
  const projectMilestoneArr: ProjectMilestoneData[]=  []
    this.apiResponse.forEach(element => {
      if (element.position >= start && element.position <= end) {
        projectMilestoneArr.push(element);
      }
    });
    this.projectMilestoneListArr = projectMilestoneArr
  }

  onEditProjectMilestoneComplete(milestoneID, value): void {
    const requestData: MarkProjectMilestoneCompleteRequestModel = new MarkProjectMilestoneCompleteRequestModel();
    requestData.id = milestoneID;
    requestData.projectID = this.projectID;
    requestData.isCompleted = value;
    this.isLoading = true;
    this.subscription = this.projectMilestoneService.updateProjectMilestoneComplete(requestData).subscribe(response => {
      this.isLoading = false;
      if (response.code === 200) {
        this.getProjectMilestoneList();
      } else {
        this.snackBar.open(response.message, 'close', {
          duration: 1500,
        });
      }
    },
    errorMessage => {
      console.log("CORS Error",errorMessage);
      if(errorMessage.statusText == "Unknown Error"){
      this.toastr.error('Some unknown error occured. Please contact administrator');
      this.isLoading = false
    }
    }
    );
  }

  onUpdateProjectMilestone(): void {
    if (this.updatedDate === null) {
      this.snackBar.open('Please Select End Date', 'close', {
        duration: 1500,
      });
      return;
    }
    const requestData: UpdateProjectMilestoneRequestModel = new UpdateProjectMilestoneRequestModel();
    requestData.id = this.currentEditingMilestone;
    requestData.projectId = this.projectID;
    requestData.plannedEndDate = this.updatedDate;
    this.isLoading = true;
    console.log('=====request data======', requestData);
    this.subscription = this.projectMilestoneService.updateProjectMilestone(requestData).subscribe(response => {
      console.log('=====check responseee=====', response);
      this.isLoading = false;
      if (response.code === 200) {
        this.getProjectMilestoneList();
      } else {
        this.snackBar.open(response.message, 'close', {
          duration: 1500,
        });
      }
    },
    errorMessage => {
      console.log("CORS Error",errorMessage);
      if(errorMessage.statusText == "Unknown Error"){
      this.toastr.error('Some unknown error occured. Please contact administrator');
      this.isLoading = false
    }
    }
    );
  }


  addEvent(event: MatDatepickerInputEvent<Date>, milestoneID): void {
    const selectedDate = moment(event.value).format('DD/MM/YYYY');
    this.updatedDate = selectedDate;
    console.log('=====type and event=====', selectedDate, milestoneID);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}















 // getProjectMilestoneList(id: string): void {
  //   this.isLoading = true;
  //   this.projectMilestoneService.getProjectMilestone(id).subscribe(response => {
  //     this.isLoading = false;
  //     if (response.code === 200) {
  //       const projectMilestoneArr: ProjectMilestoneData[] = [];
  //       if (response.data && response.data.projectMilestones) {
  //         response.data.projectMilestones.forEach(element => {
  //           const projectMilestone: ProjectMilestoneData = new ProjectMilestoneData();
  //           // if (element.position >= 0) {
  //           projectMilestone.milestoneName = element.milestoneName;
  //           if (element.assignedTo && element.assignedTo.firstName) {
  //             projectMilestone.assignedTo =
  //             element.assignedTo.firstName +
  //             (element.assignedTo.middleName ? ` ${element.assignedTo.middleName} ` : ' ') +
  //             (element.assignedTo.lastName ? ` ${element.assignedTo.lastName} ` : ' ');
  //           }
  //           if (element.team && element.team.name) {
  //             projectMilestone.teamName = element.team.name;
  //           }
  //           projectMilestone.startDate = element.startDate;
  //           projectMilestone.plannedEndDate = element.plannedEndDate;
  //           projectMilestone.markAsComplete = element.isCompleted;
  //           projectMilestone.completionDate = element.completionDate;
  //           if (element.position >= 0 && element.position <= 3) {
  //             projectMilestoneArr.push(projectMilestone);
  //           }
  //           // }
  //         });
  //       }
  //       this.projectMilestoneListArr = projectMilestoneArr;
  //     } else {
  //       this.snackBar.open(response.message, 'close', {
  //         duration: 1500,
  //       });
  //     }
  //   });
  // }
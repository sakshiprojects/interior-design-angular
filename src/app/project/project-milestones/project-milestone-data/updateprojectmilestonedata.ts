export class UpdateProjectMilestoneRequestModel {
    public id: string;
    public projectId: string;
    public plannedEndDate: string;
}

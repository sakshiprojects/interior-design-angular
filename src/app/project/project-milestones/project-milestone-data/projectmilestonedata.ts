export class ProjectMilestoneData {
    public id: string;
    public milestoneName: string;
    public assignedTo: string;
    public teamName: string;
    public startDate: string;
    public plannedEndDate: string;
    public markAsComplete: boolean;
    public completionDate: string;
    public position: number
}

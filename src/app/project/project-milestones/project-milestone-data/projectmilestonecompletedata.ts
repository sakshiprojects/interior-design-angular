export class MarkProjectMilestoneCompleteRequestModel {
    public id: string;
    public projectID: string;
    public isCompleted: boolean;
}

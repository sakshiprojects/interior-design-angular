import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';
import { LocalStorage } from 'src/app/shared/localstorage.service';
import { MarkProjectMilestoneCompleteRequestModel } from '../project-milestone-data/projectmilestonecompletedata';
import { UpdateProjectMilestoneRequestModel } from '../project-milestone-data/updateprojectmilestonedata';

@Injectable({providedIn: 'root'})
export class ProjectMilestoneService {
    constructor(private http: HttpClient, private baseURL: BaseUrl, private localStorage: LocalStorage) {}

    public getToken = this.localStorage.getAccessToken();

    getProjectMilestone(id: string): Observable<any> {
        // const getToken = this.localStorage.getAccessToken();
        const header = new HttpHeaders({'Authorization': this.getToken});
        const param = new HttpParams().set('projectID', id);
        return this.http.get(this.baseURL.getFinalUrl('getProjectMilestoneList'), {headers: header, params: param});
    }

    updateProjectMilestoneComplete(projectMilestoneCompleteData: MarkProjectMilestoneCompleteRequestModel): Observable<any> {
        // const getToken = this.localStorage.getAccessToken();
        return this.http.put(this.baseURL.getFinalUrl('markProjectMilestoneComplete'), projectMilestoneCompleteData,
        {
            headers: new HttpHeaders({ 'Authorization' : this.getToken })
        }
        );
    }

    updateProjectMilestone(updateProjectMilestoneData: UpdateProjectMilestoneRequestModel): Observable<any> {
        return this.http.put(this.baseURL.getFinalUrl('updateProjectMilestone'), updateProjectMilestoneData,
        {
            headers: new HttpHeaders({ 'Authorization' : this.getToken })
        }
        );
    }
}

import { Component, HostListener, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from './shared/localstorage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'interior-design-project';

  constructor(
    private localStorage: LocalStorage,
    private router: Router,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
    ){}

  ngOnInit(): void {
    // if (this.localStorage.getAccessToken()) {
    //   console.log(this.route);
    //   console.log('====check router====', this.router.url);
      
    //   this.router.navigate(['/home']);
    // }
    // else {
    //   this.localStorage.clearAccessToken();
    //   this.router.navigate(['/login']);
    // }


    
//     @HostListener('window:beforeunload') onbeforeunload() {
//       if (this.localStorage.getAccessToken()) {
//             this.router.navigate(['/home']);
//           } else {
//             this.localStorage.clearAccessToken();
//             this.router.navigate(['/login']);
//           }
// }
    // console.log('=======before navigation======');
    // window.onbeforeunload = (() => {
    //   console.log('=======before navigation again======');
    //   if (this.localStorage.getAccessToken()) {
    //     this.router.navigate(['/home']);
    //   } else {
    //   this.localStorage.clearAccessToken();
    //   this.router.navigate(['/login']);
    //     // this.snackBar.open('Session Expired', 'close', {
    //     //   duration: 1200,
    //     // });
    //   }
    // });
  }
}

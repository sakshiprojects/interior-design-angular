import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
// import { NgxTypeaheadModule } from "ngx-typeahead";

import { DpDatePickerModule } from 'ng2-date-picker';
import { QuillModule } from 'ngx-quill';

import { HttpClientModule } from '@angular/common/http';

import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
// import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
// import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import { HomeComponent } from './home/home.component';
import { LeadComponent } from './lead/lead.component';
import { ProjectComponent } from './project/project.component';
import { ProfileComponent } from './profile/profile.component';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';

import { MaterialModule } from './material/material.module';

import { CarouselModule } from 'ngx-owl-carousel-o';
import { LoginComponent } from './login/login.component';
import { ProfileDetailsComponent } from './profile/profile-details/profile-details.component';
import { BankAccountDetailsComponent } from './profile/bank-account-details/bank-account-details.component';
import { RecentProjectsComponent } from './profile/recent-projects/recent-projects.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ProjectLinksComponent } from './project/project-links/project-links.component';
import { ProjectDetailsComponent } from './project/project-details/project-details.component';
import { ProjectMilestonesComponent } from './project/project-milestones/project-milestones.component';
import { FilesComponent } from './project/files/files.component';
import { PaymentsComponent } from './project/payments/payments.component';
import { MomComponent } from './project/mom/mom.component';

// import { JwPaginationComponent } from 'jw-angular-pagination';



// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// import { NgbdCarouselBasic } from './carousel-basic';

// For MDB Angular Free
// import { CarouselModule, WavesModule } from 'angular-bootstrap-md';
// import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    HomeComponent,
    LeadComponent,
    ProjectComponent,
    ProfileComponent,
    LoadingSpinnerComponent,
    LoginComponent,
    ProfileDetailsComponent,
    BankAccountDetailsComponent,
    RecentProjectsComponent,
    ProjectLinksComponent,
    ProjectDetailsComponent,
    ProjectMilestonesComponent,
    FilesComponent,
    PaymentsComponent,
    MomComponent
    // JwPaginationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbModule,
    // NgxTypeaheadModule,
    DpDatePickerModule,
    QuillModule.forRoot(),
    HttpClientModule,
    FileUploadModule,
    // JwPaginationComponent,
    LayoutModule,
    MaterialModule,
    MatToolbarModule,
    // MatButtonModule,
    MatSidenavModule,
    // MatIconModule,
    MatListModule,
    // MatExpansionModule,
    CarouselModule,
    FormsModule,
    ReactiveFormsModule
    // NgbModule
    // MDBBootstrapModule.forRoot()
  ],
  // exports: [NgbdCarouselBasic],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

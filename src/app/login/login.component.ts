import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subscription } from 'rxjs';
import { LoginService } from './login-service/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseUrl } from '../shared/baseurl.service';
import { LocalStorage } from '../shared/localstorage.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public url: string;
  isLoading = false;
  // subscription: Subscription;

  constructor(
    private loginService: LoginService,
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private route: ActivatedRoute,
    private baseUrl: BaseUrl,
    private localStorage: LocalStorage,
    private snackBar: MatSnackBar,
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.onGoogleLogin();
    this.route.queryParams.subscribe(params => {
      console.log('====== check route====', this.route);
      if (params && params.code){
        this.googleLoginCallback(params.code);
      }
    });
  }

  onGoogleLogin(): void {
    this.loginService.getLogin().subscribe(data => {
      console.log('----before sending req', data);
      this.url = data.url;
      console.log('----after sending req', this.url, data);
    });
  }

  onRedirectToLogin(): void {
    // this.router.navigate(['home']);
    this.document.location.href = this.url;
  }

  googleLoginCallback(code: string): void {
    this.isLoading = true;
    this.loginService.getLoginCallback(code).subscribe(data => {
      this.isLoading = false;
      if (data.code === 200) {
        // console.log('=====RESPONSE DATA=====', data);
        this.localStorage.setAccessToken(data.userObj.loginToken);
        localStorage.setItem('officialEmail', JSON.stringify(data.userObj.designCafeEmail));
        // this.localStorage.setAccessToken(data.userObj.designCafeEmail);
        this.router.navigate(['/home']);
      } else {
        this.localStorage.clearAccessToken();
        this.router.navigate(['/login']);
        this.snackBar.open('Session Expired', 'close', {
          duration: 1500,
        });
        // alert(data.message);
      }
    },
    errorMessage => {
      console.log("CORS Error",errorMessage);
      if(errorMessage.statusText == "Unknown Error"){
      this.toastr.error('Some unknown error occured. Please contact administrator');
      this.isLoading = false
    }
    }
    );
  }

  // ngOnDestroy(): void {
  //   this.subscription.unsubscribe();
  // }

}




// this._route.paramMap.subscribe( (params: ParamMap) => {
    //   if(params){
    //   console.log('===first param===', params);
    //   console.log('=====params====', params.get('code'));
    //   }
    // });



        // // this.baseUrl.isLoggedIn = true;
        // // console.log('====params====', params.code);
        // if (this.baseUrl.isLoggedIn = false) {
        //   this.onGoogleLogin();
        //   // this.googleLoginCallback(params.code);
        // } else {
        //   // this.baseUrl.isLoggedIn = true;
        //   this.googleLoginCallback(params.code);
        //   console.log('====params====', params.code);
        // }
        // // this.baseUrl.isLoggedIn = true;l
        // // console.log('====params====', params.code);

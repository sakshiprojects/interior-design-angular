import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseUrl } from 'src/app/shared/baseurl.service';

@Injectable({providedIn: 'root'})
export class LoginService {

    constructor(private http: HttpClient, private baseUrl: BaseUrl) {}

    getLogin(): Observable<any> {
        return this.http.get(this.baseUrl.getFinalUrl('google-authorize-admin'));
    }

    getLoginCallback(code: string): Observable<any> {
        const param = new HttpParams().set('code', code);
        return this.http.get(this.baseUrl.getFinalUrl('google-authorize-admin/callback') , {params: param});
    }
}

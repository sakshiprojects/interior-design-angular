import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { BaseUrl } from '../shared/baseurl.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {

  customOptions: OwlOptions = {
    loop: true,
     mouseDrag: true,
     touchDrag: true,
     pullDrag: true,
     dots: true,
     autoplay: true,
     center: true,
     autoHeight: true,
     autoWidth: true,
     autoplayTimeout: 6000,
     autoplayHoverPause: true,
     navSpeed: 700,
     navText: ['', ''],
     responsive: {
       0: {
         items: 1
       },
       400: {
         items: 2
       },
       740: {
         items: 3
       },
       940: {
         items: 1
       }
     },
     nav: true
   };

  constructor(private baseUrl: BaseUrl, private route: ActivatedRoute) {
    // console.log('Inside call INIT ======>', this.baseUrl.isLoggedIn);
    // this.baseUrl.isLoggedIn = true;
    // console.log('Logged in value ======>', this.baseUrl.isLoggedIn);
  }

  ngOnInit(): void {
    // this.route.params.subscribe( params => {
    //   console.log('=====params====', params['code']);
    // });
    // console.log('Inside call INIT ======>', this.baseUrl.isLoggedIn);
    // this.baseUrl.isLoggedIn = true;
    // console.log('Logged in value ======>', this.baseUrl.isLoggedIn);
  }




  // images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);

}
